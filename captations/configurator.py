from copy import deepcopy


class Configurator:
    def __init__(self) -> None:
        self._configurations = {}
        self._saved = None
        self._handlers = {}

    @property
    def config_dict(self):
        return self._configurations

    @property
    def param_dict(self):
        res = {}
        for key in self._configurations:
            res[key] = self._configurations[key]["val"]
        return res

    def get_value(self, param_name):
        if param_name in self:
            return self._configurations[param_name]["val"]
        return None

    def _get_attr_config(self, nom_param):
        if nom_param in self:
            return self._configurations[nom_param]
        return None

    # -------------------
    # Ajouteurs

    def add_param(self, nom_param, val_param) -> bool:
        if nom_param not in self:
            self._configurations[nom_param] = {"val": val_param}
            return True
        return False

    def add_description(self, nom_param, description) -> bool:
        if nom_param in self:
            self._configurations[nom_param]["description"] = description
            return True
        return False

    def add_min_max(self, nom_param, min_value=None, max_value=None) -> bool:
        if nom_param in self:
            # Si l'on souhaite configurer un minimum
            if min_value is not None:
                # Vérifie si le minimum donné satisfait les contraintes actuelles
                if self.check_in_interval(nom_param, min_value):
                    self._configurations[nom_param]["min"] = min_value
                else:
                    return False

            # Si l'on souhaite configurer un maximum
            if max_value is not None:
                # Vérifie si le maximum donné satisfait les contraintes actuelles
                if self.check_in_interval(nom_param, max_value):
                    self._configurations[nom_param]["max"] = max_value
                else:
                    return False

            return True
        return False

    def add_range(self, nom_param, val_poss) -> bool:
        if nom_param in self:
            for val in val_poss:
                if not self.check_in_interval(nom_param, val):
                    return False

            self._configurations[nom_param]["range"] = val_poss
            return True

        return False

    def add_change_handler(self, nom_param, handler) -> bool:
        if nom_param in self:
            if nom_param not in self._handlers:
                self._handlers[nom_param] = []

            self._handlers[nom_param].append(handler)

            return True

        return False

    # -------------------
    # Verifieurs

    # Redéfinie l'opérateur "in" pour les objets de type Configurator
    def __contains__(self, key):
        return key in self._configurations

    def check_in_interval(self, nom_param, val) -> bool:
        conf = self._get_attr_config(nom_param)
        checked = True

        if conf:
            if "range" in conf:
                checked = checked and val in conf["range"]
            if "min" in conf:
                checked = checked and val >= conf["min"]
            if "max" in conf:
                checked = checked and val <= conf["max"]
            return checked

        return False

    def check_contraintes(self, nom_param, val) -> bool:
        return self.check_in_interval(nom_param, val)

    # -------------------
    # Actionneurs

    def apply_change(self, nom_param, val, attribute=None):
        if nom_param in self._handlers:
            if val == self._configurations[nom_param]["val"]:
                return

            for method in self._handlers[nom_param]:
                if attribute:
                    method(val, attribute)
                else:
                    method(val)

            self._configurations[nom_param]["val"] = val

    # -------------------
    # Autres

    def save(self):
        self._saved = deepcopy(self._configurations)

    def copy(self, config):
        for key in config.keys():
            if key in self._configurations.keys():
                self.apply_change(key, config[key]["val"], key)

    def reset(self):
        if self._saved is not None:
            for key in self._saved.keys():
                self.apply_change(key, self._saved[key]["val"], key)
