# Constantes générales

MAX_NUM_FACES = 1

# Niveau de confiance
MIN_DETECTION_CONFIDENCE = 0.7
MIN_TRACKING_CONFIDENCE = 0.7

FONT_UPDATE_WAIT = 5
