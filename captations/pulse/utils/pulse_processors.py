# Copyright 2017 Tristan Hearn (https://github.com/thearn)
# Copyright 2023 Nathan Menny, Laura Miguel, Simon Renaud

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import time

import cv2
import numpy as np


def resource_path(relative_path):
    """Get absolute path to resource, works for dev and for PyInstaller"""
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


# [C] S'occupe de trouver la fréquence cardiaque et le front de la personne
class FindFaceGetPulse:
    def __init__(self):
        self.frame_in = np.zeros((10, 10))
        self.frame_out = np.zeros((10, 10))
        self.fps = 0
        self.buffer_size = 250
        self.data_buffer = []
        self.times = []
        self.samples = []
        self.freqs = []
        self.fft = []
        self.t0 = time.time()
        self.bpm = 0
        dpath = resource_path("data/haarcascade_frontalface_alt.xml")
        if not os.path.exists(dpath):
            print("Cascade file not present!")
        self.face_cascade = cv2.CascadeClassifier(dpath)

        self.face_rect = [1, 1, 2, 2]
        self.last_center = np.array([0, 0])
        self.output_dim = 13

        self.idx = 1

    # [C] Permet de savoir si la position de notre front a bougé en calculant la distance entre les centres de deux rectangles
    def shift(self, detected):
        x, y, w, h = detected
        center = np.array([x + 0.5 * w, y + 0.5 * h])
        shift = np.linalg.norm(center - self.last_center)

        self.last_center = center
        return shift

    def draw_rect(self, rect, col=(0, 255, 0)):
        x, y, w, h = rect
        cv2.rectangle(self.frame_out, (x, y), (x + w, y + h), col, 1)

    # [C] Récupère un nouveau rectangle correspondant au rectangle du visage réduit par certains facteurs (les fh_quelqueChose)
    def get_subface_coord(self, fh_x, fh_y, fh_w, fh_h):
        x, y, w, h = self.face_rect
        return [
            int(x + w * fh_x - (w * fh_w / 2.0)),
            int(y + h * fh_y - (h * fh_h / 2.0)),
            int(w * fh_w),
            int(h * fh_h),
        ]

    # [C] Récupère la couleur moyenne sur l'image dans un rectangle de certaines coordonnées
    def get_subface_means(self, coord):
        x, y, w, h = coord
        subframe = self.frame_in[y : y + h, x : x + w, :]
        v1 = np.mean(subframe[:, :, 0])
        v2 = np.mean(subframe[:, :, 1])
        v3 = np.mean(subframe[:, :, 2])

        return (v1 + v2 + v3) / 3.0

    def run(self):
        self.times.append(time.time() - self.t0)
        self.frame_out = self.frame_in

        # [C] Equilibre le contraste sur une image en nuances de gris
        #       pour mieux identifier les contours des objets
        self.gray = cv2.equalizeHist(cv2.cvtColor(self.frame_in, cv2.COLOR_BGR2GRAY))
        col = (100, 255, 100)

        # [C] On recherche les visages (et donc les fronts)

        # [C] Detecte des objets de taille différente sur l'image
        detected = list(
            self.face_cascade.detectMultiScale(
                self.gray,
                scaleFactor=1.3,
                minNeighbors=4,
                minSize=(50, 50),
                flags=cv2.CASCADE_SCALE_IMAGE,
            )
        )

        if len(detected) > 0:
            # {C] Tri les objects identifiée sur l'image du plus petit au plus grand
            #       Le dernier correspond donc au plus grand élément identifié sur l'image
            #       ce qui est très certainement le front de la personne
            detected.sort(key=lambda a: a[-1] * a[-2])

            # [C] Si le dernier object identifié est décalé de plus de 10px par rapport
            #      à la dernière position que l'on avait, on décale le rectangle
            if self.shift(detected[-1]) > 10:
                self.face_rect = detected[-1]

        # [C] Si le rectangle du visage vaut celui par defaut, il n'y a rien a faire
        if set(self.face_rect) == set([1, 1, 2, 2]):
            self.reset()
            return

        # [C] Récupère le front (à partir d'un sous rectangle du rectangle de visage) et
        #      dessine son rectangle
        forehead1 = self.get_subface_coord(0.5, 0.18, 0.25, 0.15)
        self.draw_rect(forehead1)

        # [C] Récupère la couleur moyenne du front
        vals = self.get_subface_means(forehead1)

        # [C] Traite les données dans un buffer qui stocke les self.buffer_size dernière couleurs
        #       du front
        self.data_buffer.append(vals)
        L = len(self.data_buffer)
        if L > self.buffer_size:
            self.data_buffer = self.data_buffer[-self.buffer_size :]
            self.times = self.times[-self.buffer_size :]
            L = self.buffer_size

        processed = np.array(self.data_buffer)
        self.samples = processed

        # [C] A partir de 11 couleurs moyennes de front; le calcul sur la fréquence cardiaque
        #      peut se faire
        if L > 10:
            self.output_dim = processed.shape[0]

            self.fps = float(L) / (self.times[-1] - self.times[0])
            even_times = np.linspace(self.times[0], self.times[-1], L)

            try:
                # [C] On fait des opérations matématiques compliquées sur les couleurs de front
                interpolated = np.interp(even_times, self.times, processed)
                interpolated = np.hamming(L) * interpolated
                interpolated = interpolated - np.mean(interpolated)
                raw = np.fft.rfft(interpolated)
                phase = np.angle(raw)
                self.fft = np.abs(raw)
                self.freqs = float(self.fps) / L * np.arange(L / 2 + 1)

                freqs = 60.0 * self.freqs
                # [C] Récupère l'indice des données calculées où la fréquence cardiaque est entre 50 et 180 bpm
                idx = np.where((freqs > 50) & (freqs < 180))

                pruned = self.fft[idx]
                phase = phase[idx]

                pfreq = freqs[idx]
                self.freqs = pfreq
                self.fft = pruned
                idx2 = np.argmax(pruned)

                t = (np.sin(phase[idx2]) + 1.0) / 2.0
                t = 0.9 * t + 0.1
                alpha = t
                beta = 1 - t

                self.bpm = self.freqs[idx2]
                self.idx += 1
            except:
                self.reset()

    def reset(self):
        self.data_buffer, self.times, self.trained = [], [], False
