from threading import Lock

import numpy as np

from captations.general_constants import FONT_UPDATE_WAIT
from captations.pulse.utils.pulse_processors import FindFaceGetPulse
from captations.pulse.utils.pulse_constants import *
from captations.configurator import Configurator


class PulseDetector:
    def __init__(self):
        self._name = "HEART PULSE"
        self._lock = Lock()

        self.w, self.h = 0, 0

        self.font_inc = True
        self._use_color = False
        self.use = False

        self._bpm_limits = BPM_LIMITS

        # La magie se trouve là dedans
        self._processor = FindFaceGetPulse()

        # permet de contrôler certains comportements du détecteur
        # Chaque key se décompose en trois sections :
        # [
        #    le nom affiché sur l'interface,
        #    la fonction déclenchée quand la touche est reçue,
        #    <optionnel> la commande précédente qui doit être reçue pour qu'elle fonctionne
        # ]
        self._key_controls = {
            "s": ["pulse", self._enable, "use"],
            "c": ["color", self._toggle_color, "_use_color", "s"],
        }

        self._config = Configurator()

        self._config.add_param("font_min", PULSE_FONT_VAR_MIN)
        self._config.add_description(
            "font_min",
            f"Minimum variation in the font for {PULSE_VMIN_VMAX[0]} bmp",
        )
        self._config.add_min_max("font_min", min_value=0)
        self._config.add_change_handler("font_min", self._change)

        self._config.add_param("font_max", PULSE_FONT_VAR_MAX)
        self._config.add_description(
            "font_max",
            f"Maximum variation in the font for {PULSE_VMIN_VMAX[1]} bmp",
        )
        self._config.add_min_max("font_max", min_value=0)
        self._config.add_change_handler("font_max", self._change)

        self._config.save()

        # Ajoute les paramètres définis dans le Configurator comme attributs de cet objet
        for key in self._config.param_dict:
            setattr(self, key, self._config.param_dict[key])

        self.font_update_wait = FONT_UPDATE_WAIT
        self.bpms = [0] * self.font_update_wait
        self.rep = 0

    @property
    def name(self):
        return self._name

    @property
    def cmd(self):
        keys = []
        for k, v in self._key_controls.items():
            if len(v) == 4:
                keys.append((k, v[0], v[3]))
            else:
                keys.append((k, v[0]))
        return keys

    @property
    def config(self):
        return self._config.config_dict

    def get_active_cmd(self):
        return [
            key
            for key in self._key_controls.keys()
            if getattr(self, self._key_controls[key][2])
        ]

    def process_cmd(self, cmd):
        for key in self._key_controls.keys():
            if cmd == key:
                self._key_controls[key][1]()

    def process_config(self, attribute_list, new_value_list):
        counter = len(attribute_list)
        for attribute, new_value in zip(attribute_list, new_value_list):
            if attribute in self._config:
                if self._config.check_contraintes(attribute, new_value):
                    self._config.apply_change(attribute, new_value, attribute)
            counter -= 1
        return counter == 0

    def process_frame(self, frame):
        self.h, self.w, _c = frame.shape
        if not self.use:
            return frame

        self._processor.frame_in = frame
        self._processor.run()
        output_frame = self._processor.frame_out

        return output_frame

    def process_font(self, font_config, **kwargs):
        if not self.use:
            return font_config

        self._lock.acquire()

        self.bpms[self.rep] = self._processor.bpm
        self.rep = (self.rep + 1) % self.font_update_wait

        if self.rep != 0:
            self._lock.release()
            return font_config

        bpm = np.array(self.bpms).mean()

        self._lock.release()

        # vmin, vmax = self._bpm_limits
        vmin, vmax = PULSE_VMIN_VMAX

        cmin, cmax = COL_RED_LIMITS

        # Permet d'avoir une valeur entre 0 et 1
        vbpm_p = (bpm * 100 / vmax) / 100

        new_font = {
            "axe1": font_config["axe1"],
            "axe2": font_config["axe2"],
            "axe3": font_config["axe3"],
            "axe4": font_config["axe4"],
            "axe5": font_config["axe5"],
            "axe6": font_config["axe6"],
            "axe7": font_config["axe7"],
            "axe8": font_config["axe8"],
            "color": font_config["color"],
        }

        if self._use_color:
            new_font["color"][0] = int(cmax * vbpm_p + cmin * (1 - vbpm_p))
        else:
            new_font["axe3"] = (1 - vbpm_p) * font_config["axe3"]
            new_font["axe4"] = (1 - vbpm_p) * font_config["axe4"]

            if self.font_inc:
                new_font["axe3"] += vbpm_p * self.font_max
                new_font["axe4"] += vbpm_p * self.font_max
            else:
                new_font["axe3"] += vbpm_p * self.font_min
                new_font["axe4"] += vbpm_p * self.font_min

            self.font_inc ^= True

        return new_font

    def process_reset_config(self):
        self._config.reset()

    def process_copy_config(self, config):
        self._config.copy(config)

    def _enable(self):
        if not self.use:
            self._processor.reset()
        self.use = not self.use

    def _toggle_color(self):
        self._use_color ^= True

    def _change(self, new_value, attribute):
        self._lock.acquire()
        setattr(self, attribute, new_value)
        self._lock.release()
