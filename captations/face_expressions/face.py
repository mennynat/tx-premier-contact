import cv2
import numpy as np
from scipy.spatial import distance as dist


def brows_expression(face):
    eyeL_bottom = face[253]
    eyeR_bottom = face[23]

    # Récupère les différentes informations sur le sourcil gauche
    browL_top = face[443]
    browL_eyeL_upper_dist = dist.euclidean(
        [browL_top.x, browL_top.y], [eyeL_bottom.x, eyeL_bottom.y]
    )
    browL_bottom = face[257]
    browL_eyeL_lower_dist = dist.euclidean(
        [browL_bottom.x, browL_bottom.y], [eyeL_bottom.x, eyeL_bottom.y]
    )
    browL_eyeL_dist = (browL_eyeL_lower_dist + browL_eyeL_upper_dist) / 2

    # Récupère les différentes informations sur le sourcil droit
    browR_top = face[52]
    browR_eyeR_upper_dist = dist.euclidean(
        [browR_top.x, browR_top.y], [eyeR_bottom.x, eyeR_bottom.y]
    )
    browR_bottom = face[223]
    browR_eyeR_lower_dist = dist.euclidean(
        [browR_bottom.x, browR_bottom.y], [eyeR_bottom.x, eyeR_bottom.y]
    )
    browR_eyeR_dist = (browR_eyeR_lower_dist + browR_eyeR_upper_dist) / 2

    face_mid_right = face[234]
    face_mid_left = face[454]
    brows_avg_raise = (browR_eyeR_dist + browL_eyeL_dist) / (
        face_mid_left.x - face_mid_right.x
    )

    return brows_avg_raise


def distance(face):
    nose = face[1]
    return nose.z


def head_rotation(face, img_h, img_w):
    eyeR_outer = face[33]
    eyeL_outer = face[263]
    mouth_outer_right = face[61]
    mouth_outer_left = face[291]
    nose = face[1]
    chin = face[199]

    face_3d = []
    face_2d = []

    for lm in [eyeR_outer, eyeL_outer, mouth_outer_right, mouth_outer_left, nose, chin]:
        x, y = int(lm.x * img_w), int(lm.y * img_h)
        face_2d.append([x, y])
        face_3d.append([x, y, lm.z])

    face_2d = np.array(face_2d, dtype=np.float64)
    face_3d = np.array(face_3d, dtype=np.float64)
    focal_length = 1 * img_w
    cam_matrix = np.array(
        [[focal_length, 0, img_h / 2], [0, focal_length, img_w / 2], [0, 0, 1]]
    )
    dist_matrix = np.zeros((4, 1), dtype=np.float64)
    _, rot_vec, _, _ = cv2.solvePnPRansac(face_3d, face_2d, cam_matrix, dist_matrix)
    rmat, _ = cv2.Rodrigues(rot_vec)
    angles, _, _, _, _, _ = cv2.RQDecomp3x3(rmat)

    x = angles[0] * 360
    y = angles[1] * 360

    return x, y


def mouth_expression(face):
    face_mid_right = face[234]
    face_mid_left = face[454]

    # Récupère les différentes informations sur l'extérieur de la bouche
    mouth_outer_right = face[61]
    mouth_outer_left = face[291]

    # Récupère les différentes informations sur l'intérieur de la bouche
    mouth_inner_top = face[13]
    mouth_inner_bottom = face[14]

    mouth_width = (mouth_outer_right.x - mouth_outer_left.x) / (
        face_mid_right.x - face_mid_left.x
    )

    mouth_inner_height = dist.euclidean(
        [mouth_inner_top.x, mouth_inner_top.y],
        [mouth_inner_bottom.x, mouth_inner_bottom.y],
    )

    return mouth_width, mouth_inner_height
