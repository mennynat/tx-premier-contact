# Constantes liées aux expressions du visage

# Sourcils
BROWS_DOWN = 0.35
BROWS_UP = 0.58

# Distance nez-écran
# NOSE_DIST_MIN = -0.11
# NOSE_DIST_MAX = -0.02
NOSE_DIST_MIN = -0.067
NOSE_DIST_MAX = -0.052

# Tête
HEAD_Y_MIN = -30
HEAD_Y_MID_MIN = -10
HEAD_Y_MID_MAX = 10
HEAD_Y_MAX = 25

HEAD_X_MIN = -5
HEAD_X_MID_MIN = 10
HEAD_X_MID_MAX = 20
HEAD_X_MAX = 35

# Bouche
OPEN_MIN = 0
OPEN_MAX = 0.1
SMILE_MIN = 0.24
SMILE_MAX = 0.52

# Valeurs pour les axes et la font : (défaut, à ajouter au défaut pour obtenir le max)
FONT_SIZE = (500, 100)
AXE = (100, 1000)
