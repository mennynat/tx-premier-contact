import cv2
import mediapipe.python.solutions.face_mesh as mp_face_mesh
import numpy as np

from captations.face_expressions.face import (
    brows_expression,
    distance,
    head_rotation,
    mouth_expression,
)
from captations.general_constants import (
    MAX_NUM_FACES,
    MIN_DETECTION_CONFIDENCE,
    MIN_TRACKING_CONFIDENCE,
)
from captations.face_expressions.utils.expressions_constants import (
    BROWS_DOWN,
    BROWS_UP,
    SMILE_MIN,
    SMILE_MAX,
    OPEN_MIN,
    OPEN_MAX,
    HEAD_Y_MIN,
    HEAD_Y_MAX,
    HEAD_Y_MID_MIN,
    HEAD_Y_MID_MAX,
    HEAD_X_MIN,
    HEAD_X_MAX,
    HEAD_X_MID_MIN,
    HEAD_X_MID_MAX,
    NOSE_DIST_MIN,
    NOSE_DIST_MAX,
    FONT_SIZE,
    AXE,
)


class FaceExpressions:
    def __init__(self):
        self._face_mesh = mp_face_mesh.FaceMesh(
            static_image_mode=True,
            max_num_faces=MAX_NUM_FACES,
            refine_landmarks=True,
            min_detection_confidence=MIN_DETECTION_CONFIDENCE,
            min_tracking_confidence=MIN_TRACKING_CONFIDENCE,
        )

        self.frame_in = np.zeros((10, 10))
        self.frame_out = np.zeros((10, 10))

        self.frown = 0
        self.smile = 0
        self.open_mouth = 0
        self.head_left = 0
        self.head_right = 0
        self.head_up = 0
        self.head_down = 0
        self.distance = 0
        self.brows = False
        self.mouth = False
        self.head = False
        self.dist = False

    def use_brows(self):
        self.brows = not self.brows
        return self.brows

    def use_mouth(self):
        self.mouth = not self.mouth
        return self.mouth

    def use_distance(self):
        self.dist = not self.dist
        return self.dist

    def use_head(self):
        self.head = not self.head
        return self.head

    def run(
        self,
        brows_down=BROWS_DOWN,
        brows_up=BROWS_UP,
        smile_min=SMILE_MIN,
        smile_max=SMILE_MAX,
        open_mouth_min=OPEN_MIN,
        open_mouth_max=OPEN_MAX,
        y_min=HEAD_Y_MIN,
        y_max=HEAD_Y_MAX,
        x_min=HEAD_X_MIN,
        x_max=HEAD_X_MAX,
        dist_min=NOSE_DIST_MIN,
        dist_max=NOSE_DIST_MAX,
        variables_param={
            "brows": AXE,
            "smile": AXE,
            "open_mouth": AXE,
            "head_left": AXE,
            "head_right": AXE,
            "head_up": AXE,
            "head_down": AXE,
            "distance": FONT_SIZE,
        },
    ):
        self.frame_out = self.frame_in
        self.h, self.w, _ = self.frame_in.shape

        self.frame_out = cv2.cvtColor(self.frame_out, cv2.COLOR_BGR2RGB)
        # Process l'image à l'aide de mediapipe
        results = self._face_mesh.process(self.frame_out)

        self.frame_out = cv2.cvtColor(self.frame_out, cv2.COLOR_RGB2BGR)

        if results.multi_face_landmarks and len(results.multi_face_landmarks) > 0:
            face_landmarks = results.multi_face_landmarks[0]
            face = face_landmarks.landmark

            if self.brows:
                frown = brows_expression(face)
                self.frown = variables_param["brows"][0] + (frown - brows_down) * (
                    variables_param["brows"][1] / (brows_up - brows_down)
                )

            if self.mouth:
                smile, open_mouth = mouth_expression(face)
                self.smile = variables_param["smile"][0] + (smile - smile_min) * (
                    variables_param["smile"][1] / (smile_max - smile_min)
                )
                self.open_mouth = variables_param["open_mouth"][0] + (
                    open_mouth - open_mouth_min
                ) * (
                    variables_param["open_mouth"][1] / (open_mouth_max - open_mouth_min)
                )

            if self.head:
                x, y = head_rotation(face, self.h, self.w)
                if y > HEAD_Y_MID_MIN and y < HEAD_Y_MID_MAX:
                    self.head_left = variables_param["head_left"][0]
                    self.head_right = variables_param["head_right"][0]
                elif y < HEAD_Y_MID_MIN:
                    self.head_left = -(
                        variables_param["head_left"][0]
                        + (y - HEAD_Y_MID_MIN)
                        * (variables_param["head_left"][1] / (HEAD_Y_MID_MIN - y_min))
                    )
                    self.head_right = variables_param["head_right"][0]
                else:
                    self.head_left = variables_param["head_left"][0]
                    self.head_right = variables_param["head_right"][0] + (
                        y - HEAD_Y_MID_MAX
                    ) * (variables_param["head_right"][1] / (y_max - HEAD_Y_MID_MAX))

                if x > HEAD_X_MID_MIN and x < HEAD_X_MID_MIN:
                    self.head_down = variables_param["head_down"][0]
                    self.head_up = variables_param["head_up"][0]
                elif x < HEAD_X_MID_MIN:
                    self.head_down = -(
                        variables_param["head_down"][0]
                        + (x - HEAD_X_MID_MIN)
                        * (variables_param["head_down"][1] / (HEAD_X_MID_MIN - x_min))
                    )
                    self.head_up = variables_param["head_up"][0]
                else:
                    self.head_down = variables_param["head_down"][0]
                    self.head_up = variables_param["head_up"][0] + (
                        x - HEAD_X_MID_MAX
                    ) * (variables_param["head_up"][1] / (x_max - HEAD_X_MID_MAX))

            if self.dist:
                z = distance(face)
                self.distance = variables_param["distance"][0] + (z - dist_max) * (
                    variables_param["distance"][1] / (dist_min - dist_max)
                )

        self.frame_out = cv2.flip(self.frame_out, 1)
