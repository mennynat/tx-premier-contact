from threading import Lock

from captations.face_expressions.utils.expressions_processor import FaceExpressions
from captations.configurator import Configurator
from captations.face_expressions.utils.expressions_constants import (
    BROWS_DOWN,
    BROWS_UP,
    SMILE_MIN,
    SMILE_MAX,
    OPEN_MIN,
    OPEN_MAX,
    HEAD_Y_MIN,
    HEAD_Y_MAX,
    HEAD_X_MIN,
    HEAD_X_MAX,
    HEAD_Y_MID_MIN,
    HEAD_Y_MID_MAX,
    HEAD_X_MID_MIN,
    HEAD_X_MID_MAX,
    NOSE_DIST_MIN,
    NOSE_DIST_MAX,
    FONT_SIZE,
    AXE,
)


class ExpressionsDetector:
    def __init__(self):
        self._name = "EXPRESSIONS"

        self._processor = FaceExpressions()

        self._lock = Lock()
        self.w, self.h = 0, 0

        self.use_brows = False
        self.use_mouth = False
        self.use_head = False
        self.use_distance = False

        self._key_controls = {
            "s": ["eyebrows", self._enable_brows, "use_brows"],
            "b": ["mouth", self._enable_mouth, "use_mouth"],
            "d": ["head distance", self._enable_distance, "use_distance"],
            "t": ["head move", self._enable_head, "use_head"],
        }

        self._config = Configurator()
        self._config.add_param("brows_down", BROWS_DOWN)
        self._config.add_description(
            "brows_down", "Distance when eyebrows are furrowed"
        )
        self._config.add_min_max("brows_down", min_value=0)
        self._config.add_change_handler("brows_down", self._change)

        self._config.add_param("brows_up", BROWS_UP)
        self._config.add_description("brows_up", "Distance when eyebrows are raised")
        self._config.add_min_max("brows_up", min_value=0)
        self._config.add_change_handler("brows_up", self._change)

        self._config.add_param("open_mouth_max", OPEN_MAX)
        self._config.add_description("open_mouth_max", "Maximum height for the mouth")
        self._config.add_min_max("open_mouth_max", min_value=0)
        self._config.add_change_handler("open_mouth_max", self._change)

        self._config.add_param("open_mouth_min", OPEN_MIN)
        self._config.add_description("open_mouth_min", "Minimum height for the mouth")
        self._config.add_min_max("open_mouth_min", min_value=0)
        self._config.add_change_handler("open_mouth_min", self._change)

        self._config.add_param("smile_max", SMILE_MAX)
        self._config.add_description("smile_max", "Maximum width for the mouth")
        self._config.add_min_max("smile_max", min_value=0)
        self._config.add_change_handler("smile_max", self._change)

        self._config.add_param("smile_min", SMILE_MIN)
        self._config.add_description("smile_min", "Minimum width for the mouth")
        self._config.add_min_max("smile_min", min_value=0)
        self._config.add_change_handler("smile_min", self._change)

        self._config.add_param("distance_max", NOSE_DIST_MAX)
        self._config.add_description(
            "distance_max",
            "Maximum nose-to-screen distance",
        )
        self._config.add_change_handler("distance_max", self._change)

        self._config.add_param("distance_min", NOSE_DIST_MIN)
        self._config.add_description(
            "distance_min",
            "Minimum nose-to-screen distance",
        )
        self._config.add_change_handler("distance_min", self._change)

        self._config.add_param("head_x_max", HEAD_X_MAX)
        self._config.add_description(
            "head_x_max",
            "Maximum vertical head rotation angle",
        )
        self._config.add_min_max("head_x_max", min_value=HEAD_X_MID_MAX)
        self._config.add_change_handler("head_x_max", self._change)

        self._config.add_param("head_x_min", HEAD_X_MIN)
        self._config.add_description(
            "head_x_min",
            "Minimum vertical head rotation angle",
        )
        self._config.add_min_max("head_x_min", max_value=HEAD_X_MID_MIN)
        self._config.add_change_handler("head_x_min", self._change)

        self._config.add_param("head_y_max", HEAD_Y_MAX)
        self._config.add_description(
            "head_y_max",
            "Maximum horizontal head rotation angle",
        )
        self._config.add_min_max("head_y_max", min_value=HEAD_Y_MID_MAX)
        self._config.add_change_handler("head_y_max", self._change)

        self._config.add_param("head_y_min", HEAD_Y_MIN)
        self._config.add_description(
            "head_y_min",
            "Minimum horizontal head rotation angle",
        )
        self._config.add_min_max("head_y_min", max_value=HEAD_Y_MID_MIN)
        self._config.add_change_handler("head_y_min", self._change)

        self._config.add_param("variable_brows", "axe2")
        self._config.add_description(
            "variable_brows", "Font variable associated with eyebrows"
        )
        self._config.add_range(
            "variable_brows",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_brows", self._change)

        self._config.add_param("variable_open_mouth", "axe4")
        self._config.add_description(
            "variable_open_mouth",
            "Font variable associated with the mouth height",
        )
        self._config.add_range(
            "variable_open_mouth",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_open_mouth", self._change)

        self._config.add_param("variable_smile", "axe3")
        self._config.add_description(
            "variable_smile", "Font variable associated with the mouth width"
        )
        self._config.add_range(
            "variable_smile",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_smile", self._change)

        self._config.add_param("variable_head_up", "axe3")
        self._config.add_description(
            "variable_head_up",
            "Font variable associated with the head when it's up",
        )
        self._config.add_range(
            "variable_head_up",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_head_up", self._change)

        self._config.add_param("variable_head_down", "axe4")
        self._config.add_description(
            "variable_head_down",
            "Font variable associated with the head when it's down",
        )
        self._config.add_range(
            "variable_head_down",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_head_down", self._change)

        self._config.add_param("variable_head_right", "axe2")
        self._config.add_description(
            "variable_head_right",
            "Font variable associated with the right-hand profile",
        )
        self._config.add_range(
            "variable_head_right",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_head_right", self._change)

        self._config.add_param("variable_head_left", "axe1")
        self._config.add_description(
            "variable_head_left",
            "Font variable associated with the left-hand profile",
        )
        self._config.add_range(
            "variable_head_left",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_head_left", self._change)

        self._config.add_param("variable_distance", "font_size")
        self._config.add_description(
            "variable_distance",
            "Variable de la font associée à la distance à l'écran",
        )
        self._config.add_range(
            "variable_distance",
            [
                "axe1",
                "axe2",
                "axe3",
                "axe4",
                "axe5",
                "axe6",
                "axe7",
                "axe8",
                "font_size",
            ],
        )
        self._config.add_change_handler("variable_distance", self._change)

        self._config.save()

        # Ajoute les paramètres définis dans le Configurator comme attributs de cet objet
        for key in self._config.param_dict:
            setattr(self, key, self._config.param_dict[key])

    @property
    def name(self):
        return self._name

    @property
    def cmd(self):
        keys = []
        for k, v in self._key_controls.items():
            keys.append((k, v[0]))

        return keys

    @property
    def config(self):
        return self._config.config_dict

    def get_active_cmd(self):
        return [
            key
            for key in self._key_controls.keys()
            if getattr(self, self._key_controls[key][2])
        ]

    def process_cmd(self, cmd):
        for key in self._key_controls.keys():
            if cmd == key:
                self._key_controls[key][1]()

    def process_config(self, attribute_list, new_value_list):
        counter = len(attribute_list)
        for attribute, new_value in zip(attribute_list, new_value_list):
            if attribute in self._config:
                if self._config.check_contraintes(attribute, new_value):
                    self._config.apply_change(attribute, new_value, attribute)
            counter -= 1
        return counter == 0

    def process_frame(self, frame):
        self.h, self.w, _c = frame.shape

        self._lock.acquire()

        self._processor.frame_in = frame
        variables_param = {
            variable.replace("variable_", ""): FONT_SIZE
            if (variable == "font_size")
            else AXE
            for variable in self.__dir__()
            if "variable_" in variable
        }
        self._processor.run(
            brows_down=self.brows_down,
            brows_up=self.brows_up,
            smile_min=self.smile_min,
            smile_max=self.smile_max,
            open_mouth_min=self.open_mouth_min,
            open_mouth_max=self.open_mouth_max,
            y_min=self.head_y_min,
            y_max=self.head_y_max,
            x_min=self.head_x_min,
            x_max=self.head_x_max,
            dist_min=self.distance_min,
            dist_max=self.distance_max,
            variables_param=variables_param,
        )
        output_frame = self._processor.frame_out

        self._lock.release()

        return output_frame

    def process_font(self, font_config, **kwargs):
        if (
            not self.use_brows
            and not self.use_mouth
            and not self.use_head
            and not self.use_distance
        ):
            return font_config

        new_font = {
            "axe1": font_config["axe1"],
            "axe2": font_config["axe2"],
            "axe3": font_config["axe3"],
            "axe4": font_config["axe4"],
            "axe5": font_config["axe5"],
            "axe6": font_config["axe6"],
            "axe7": font_config["axe7"],
            "axe8": font_config["axe8"],
            "color": font_config["color"],
            "font_size": font_config["font_size"],
        }

        if self.use_brows:
            new_font[self.variable_brows] = self._processor.frown
        if self.use_mouth:
            new_font[self.variable_smile] = self._processor.smile
            new_font[self.variable_open_mouth] = self._processor.open_mouth
        if self.use_head:
            new_font[self.variable_head_left] = self._processor.head_left
            new_font[self.variable_head_right] = self._processor.head_right
            new_font[self.variable_head_up] = self._processor.head_up
            new_font[self.variable_head_down] = self._processor.head_down
        if self.use_distance:
            new_font[self.variable_distance] = self._processor.distance

        return new_font

    def process_reset_config(self):
        self._config.reset()

    def process_copy_config(self, config):
        self._config.copy(config)

    def _enable_brows(self):
        self.use_brows = not self.use_brows
        self._processor.use_brows()

    def _enable_mouth(self):
        self.use_mouth = not self.use_mouth
        self._processor.use_mouth()

    def _enable_distance(self):
        self.use_distance = not self.use_distance
        self._processor.use_distance()

    def _enable_head(self):
        self.use_head = not self.use_head
        self._processor.use_head()

    def _change(self, new_value, attribute):
        self._lock.acquire()
        setattr(self, attribute, new_value)
        self._lock.release()
