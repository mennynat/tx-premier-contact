const FPS = 10

class Camera {
    constructor(videoElement, fps = FPS) {
        if (!'mediaDevices' in navigator || !'getUserMedia' in navigator.mediaDevices) {
            throw new Error("MediaDevices not available for this navigator")
        }

        this.webcamConstraints = {
            video: {
                width: 400,
                height: 300
            }
        };

        this.fps = fps;
        this.videoElt = videoElement
        this.stream = null

        videoElement.width = 400;
        videoElement.height = 300;
    }

    async getCameraSelection(htmlElement) {
        const devices = await navigator.mediaDevices.enumerateDevices();
        const videoDevices = devices.filter(device => device.kind === 'videoinput');
        let cnt = 0
        const options = videoDevices.map(videoDevice => {
            return `<option value="${videoDevice.deviceId}">Camera${cnt++}</option>`;
        });
        htmlElement.innerHTML = options.join('');
    }

    ready() {
        return this.stream != null;
    }

    recording() {
        return this.ready() && !this.videoElt.paused;
    }

    async start(deviceId) {
        if (!this.stream) {
            const webcamConstraints = {
                ...this.webcamConstraints,
                deviceId: {
                    exact: deviceId
                }
            };
            this.stream = await navigator.mediaDevices.getUserMedia(webcamConstraints);
            this.videoElt.srcObject = this.stream;
        }

        if (this.videoElt.paused) {
            this.videoElt.play();
        }
    }

    pause() {
        if (this.stream && !this.videoElt.paused) {
            this.videoElt.pause();
        }
    }

    stop() {
        if (this.stream) {
            this.videoElt.pause();
            this.videoElt.currentTime = 0;

            this.stream.getTracks().forEach((track) => {
                if (track.readyState == 'live') {
                    track.stop();
                }
            });

            this.stream = null;
        }
    }
}
