const defaultFontSize = 500
let shareConfiguration = true;
let currentFont = "OrganicformVF";

// Les elements sur la page

const usrFonts = {
  "usr1": document.getElementById("usr1"),
  "usr2": document.getElementById("usr2"),
};

const usrPseudo = {
  "usr1": document.getElementById("usr1_pseudo"),
  "usr2": document.getElementById("usr2_pseudo"),
};

const usrNoCam = {
  "usr1": document.getElementById("usr1_no_cam"),
  "usr2": document.getElementById("usr2_no_cam"),
};

const usrVideoContainer = {
  "usr1": document.getElementById("usr1_video_container"),
  "usr2": document.getElementById("usr2_video_container"),
};

const usrPhoto = {
  "usr1": document.getElementById("usr1_photo"),
  "usr2": document.getElementById("usr2_photo"),
};

const hideBtn = document.getElementById("hide");
const fullscreen = document.getElementById("fullscreen_icon");
const config = document.getElementById("config_icon");
const info = document.getElementById("info_icon");
const formSize = document.getElementById("form_size");

const elem = document.documentElement;

// Handler

hideBtn.addEventListener("click", () => {
  if (hideBtn.textContent == "show") {
    hideBtn.textContent = "hide";
    changePictoVisibility(fullscreen);
    changePictoVisibility(config);
    changePictoVisibility(info);
  } else {
    hideBtn.textContent = "show";
    changePictoVisibility(fullscreen);
    changePictoVisibility(config);
    changePictoVisibility(info);
  }
});

fullscreen.addEventListener("click", () => {
  if (fullscreen.textContent == "$") {
    openFullscreen();
  } else {
    closeFullscreen();
  }
});

// Pour tous les menus définis, on ajoute un évènement pour quitter le menu
for (let elt of document.getElementsByClassName("overlay")) {
  elt.addEventListener("click", (evt) => {

    // Si l'élément cliqué correspond au vide autour du menu
    if (evt.target.classList.contains("overlay")) {
      document.location = "#";
    }
  });
}

// Change le pictogramme pour mettre en plein écran dans le cas ou l'on quitte le plein écran avec ECHAP
document.addEventListener("fullscreenchange", (evt) => {
  if (fullscreen.textContent == "$") {
    fullscreen.textContent = "%";
  } else {
    fullscreen.textContent = "$";
  }
});

function changePictoVisibility(elt) {
  elt.classList.toggle("show");
  elt.classList.toggle("hide");
}

function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) { /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE11 */
    document.msExitFullscreen();
  }
}


// Le websocket

const socket = io.connect(window.location.protocol + "//" + document.domain + ":" + location.port);

async function animateFont(what, axe1, axe2, axe3, axe4, axe5, axe6, axe7, axe8, color, fontSize) {
  let animationFrame = [
    {
      "fontVariationSettings": `\"axe1\" ${axe1}, \"axe2\" ${axe2}, \"axe3\" ${axe3}, \"axe4\" ${axe4}, \"axe5\" ${axe5}, \"axe6\" ${axe6}, \"axe7\" ${axe7}, \"axe8\" ${axe8}`,
      "color": `rgb(${color[0]}, ${color[1]}, ${color[2]})`,
      "fontSize": `${fontSize}pt`
    }
  ];

  let animationTiming = {
    "duration": 1000 / 10,
    "fill": "forwards"
  };

  const animation = what.animate(animationFrame, animationTiming);

  await animation.finished
  animation.commitStyles();
  animation.cancel();
}

socket.addEventListener("connect", () => {
  console.log("connected !!");
});

socket.addEventListener("disconnect", () => {
  console.log("disconnected !!");
});

socket.addEventListener("observed", (data) => {
  for (let usrName in data) {
    const data_usr = data[usrName];
    if ("img" in data_usr) {
      if (usrFonts[usrName].classList.contains("hidden") && usrVideoContainer[usrName].classList.contains("hidden")) {
        usrNoCam[usrName].classList.add("hidden");
        usrNoCam[usrName].classList.remove("circle");
        usrVideoContainer[usrName].classList.remove("hidden");
      }
      usrPhoto[usrName].setAttribute("src", data_usr.img);
    }
    if ("font" in data_usr) {
      const size = data_usr.font.font_size + parseInt(formSize.value, 10) - defaultFontSize;
      animateFont(usrFonts[usrName], data_usr.font.axe1, data_usr.font.axe2, data_usr.font.axe3, data_usr.font.axe4, data_usr.font.axe5, data_usr.font.axe6, data_usr.font.axe7, data_usr.font.axe8, data_usr.font.color, size);
    }
  }
});

socket.addEventListener("pseudos", (data) => {
  for (let usrName in data) {
    usrPseudo[usrName].textContent = data[usrName];
  }
});

socket.addEventListener("cam_status", (data) => {
  for (let usrName in data) {
    const data_usr = data[usrName];
    if (data_usr == "no-cam") {
      usrNoCam[usrName].classList.remove("hidden");
      usrNoCam[usrName].classList.add("circle");
      usrVideoContainer[usrName].classList.add("hidden");
      usrFonts[usrName].classList.add("hidden");
    } else if (data_usr == "video") {
      usrNoCam[usrName].classList.add("hidden");
      usrNoCam[usrName].classList.remove("circle");
      usrVideoContainer[usrName].classList.remove("hidden");
      usrFonts[usrName].classList.add("hidden");
    } else {
      usrNoCam[usrName].classList.add("hidden");
      usrNoCam[usrName].classList.remove("circle");
      usrVideoContainer[usrName].classList.add("hidden");
      usrFonts[usrName].classList.remove("hidden");
    }
  }
});

socket.addEventListener("font_file", async (data) => {
  for (let usrName in data) {
    const data_usr = data[usrName];
    let fontName = data_usr["name"];
    if (fontName != "OrganicformVF") {
      font = new FontFace(fontName, data_usr["data"]);
      document.fonts.add(font);
      await font.load();
    }
    if (data[usrName]["shares"]) {
      currentFont = `${fontName}`;
      for (usr in usrFonts) {
        usrFonts[usr].style.fontFamily = `${fontName}`;
      }
    } else {
      usrFonts[usrName].style.fontFamily = `${fontName}`;
    }
  }
});

/* -----------
      Disconnect Other
   -----------*/

socket.addEventListener("disconnect_other", (data) => {
  usr = data.user;
  usrNoCam[usr].classList.add("hidden");
  usrNoCam[usr].classList.remove("circle");
  usrVideoContainer[usr].classList.add("hidden");
  usrPseudo[usr].textContent = " ";
  usrFonts[usr].classList.remove("hidden");
  if (shareConfiguration && data.is_first) {
    usrFonts[usr].style.fontFamily = "OrganicformVF";
  } else {
    usrFonts[usr].style.fontFamily = currentFont;
  }
  animateFont(usrFonts[usr], 100, 100, 100, 100, 100, 100, 100, 100, [0, 0, 0], parseInt(formSize.value, 10));
});

// Change size
formSize.addEventListener("change", () => {
  if (!((parseInt(formSize.value, 10) > 100) && (parseInt(formSize.value, 10) < 1000)))
    formSize.value = 500;
  usrFonts["usr1"].style.fontSize = `${formSize.value}pt`;
  usrFonts["usr2"].style.fontSize = `${formSize.value}pt`;
  usrPhoto["usr1"].style.width = `${formSize.value}pt`;
  usrPhoto["usr1"].style.height = `${formSize.value}pt`;
  usrPhoto["usr2"].style.width = `${formSize.value}pt`;
  usrPhoto["usr2"].style.height = `${formSize.value}pt`;
  usrNoCam["usr1"].style.width = `${formSize.value}pt`;
  usrNoCam["usr1"].style.height = `${formSize.value}pt`;
  usrNoCam["usr2"].style.width = `${formSize.value}pt`;
  usrNoCam["usr2"].style.height = `${formSize.value}pt`;
});

socket.addEventListener("changeShareStatus", () => {
  shareConfiguration = !shareConfiguration;
});