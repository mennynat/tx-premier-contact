const defaultFontSize = 500;
const defaultFontSizeInVh = 60;

const socket = io.connect(window.location.protocol + "//" + document.domain + ":" + location.port);
let interval = null
let currentFont = "OrganicformVF"

function setFontSettings(axe1, axe2, axe3, axe4, axe5, axe6, axe7, axe8, color, fontSize) {
    myFontArea.style.fontVariationSettings = `\"axe1\" ${axe1}, \"axe2\" ${axe2}, \"axe3\" ${axe3}, \"axe4\" ${axe4}, \"axe5\" ${axe5}, \"axe6\" ${axe6}, \"axe7\" ${axe7}, \"axe8\" ${axe8}`;
    myFontArea.style.color = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
    myFontArea.style.fontSize = `${computeFontSizeInVh(fontSize)}vh`;
}

async function animateFont(what, axe1, axe2, axe3, axe4, axe5, axe6, axe7, axe8, color, fontSize) {
    let animationFrame = [
        {
            "fontVariationSettings": `\"axe1\" ${axe1}, \"axe2\" ${axe2}, \"axe3\" ${axe3}, \"axe4\" ${axe4}, \"axe5\" ${axe5}, \"axe6\" ${axe6}, \"axe7\" ${axe7}, \"axe8\" ${axe8}`,
            "color": `rgb(${color[0]}, ${color[1]}, ${color[2]})`,
            "fontSize": `${computeFontSizeInVh(fontSize)}vh`
        }
    ];

    let animationTiming = {
        "duration": 1000 / FPS,
        "fill": "forwards"
    };

    const animation = what.animate(animationFrame, animationTiming);

    await animation.finished
    try {
        animation.commitStyles();
        animation.cancel();
    } catch (e) { }
}

function genCmdSender(btn, span, methodName, cmd, cmdName) {
    return () => {
        const data = { "method": methodName, "cmd": cmd, "cmdName": cmdName }
        if (shareConfiguration) {
            data["shares"] = true;
        }
        socket.emit("cmd", data);
        changeElementText(btn);
        changeElementColor(span);
    }
}

socket.addEventListener("cmd", (data) => {
    btnElt = document.getElementById(data + "-btn");
    spanElt = document.getElementById(data + "-span");

    if (!sharesWithOther()) {
        btnElt.disabled = false;
        btnElt.click();
        btnElt.disabled = true;
        if (btnElt.dataset.linkedBtn) {
            linkedBtn = document.getElementById(btnElt.dataset.linkedBtn);
            linkedBtn.disabled = true;
        }
    }
});

socket.addEventListener("connect", () => {
    console.log("connected !!");

    interval = setInterval(sendImage, 1000 / FPS);
});

socket.addEventListener("disconnect", () => {
    console.log("disconnected !!");
    clearInterval(interval);
    cmdMenu.innerHTML = "";
    confAlgo.innerHTML = "";
});

socket.addEventListener("init", (data) => {

    if ("is_first" in data) {
        for (elt of shareConfigBtnGlb) {
            elt.classList.remove("hidden");
        }
        myFontArea.style.fontFamily = "OrganicformVF";
    }

    let config;
    if (data.config) {
        config = data.config;
    } else {
        config = data.init.config;
    }

    let keys = data.init.keys;

    font = data.init.font;
    const size = font.font_size + parseInt(smallForm.value, 10) - defaultFontSize;
    setFontSettings(100, 100, 100, 100, 100, 100, 100, 100, [0, 0, 0], size);

    formCam.checked = false;
    if (sharesWithOther()) {
        shareConfigBtn.checked = shareConfiguration;
    } else {
        shareConfiguration = false;
    }

    // Construit la liste des commandes des différentes méthodes de 
    // captation pour que l'on puisse interagir avec
    for (method in keys) {
        let newMethodElt = document.createElement("br");
        cmdMenu.appendChild(newMethodElt);

        newMethodElt = document.createElement("div");

        let pElt = document.createElement("p");
        pElt.textContent = method;

        newMethodElt.appendChild(pElt);

        let gridElt = document.createElement("div");
        gridElt.classList.add("grid-wrapper");

        const savedBtn = {};

        for (cmd of keys[method]) {
            let divElt = document.createElement("div");
            divElt.textContent = cmd[1];
            divElt.classList.add("button-title");
            gridElt.appendChild(divElt);

            divElt = document.createElement("div");
            gridElt.appendChild(divElt);

            divElt = document.createElement("div");
            let btnElt = document.createElement("button");
            btnElt.textContent = "activate";
            btnElt.id = cmd[1] + "-btn";
            btnElt.classList.add("btn-activate-captation");
            divElt.appendChild(btnElt);
            gridElt.appendChild(divElt);

            divElt = document.createElement("div");
            let spanElt = document.createElement("span");
            spanElt.textContent = "3";
            spanElt.id = cmd[1] + "-span";
            spanElt.classList.add("picto-activate-captation");
            spanElt.classList.add("font-picto");
            spanElt.classList.add("btn-deactivated");
            divElt.appendChild(spanElt);
            gridElt.appendChild(divElt);

            if (data.cmd && data.cmd[`${method}`].includes(cmd[0])) {
                changeElementText(btnElt);
                changeElementColor(spanElt);
            }

            // Si la commande courante a besoin d'une autre commandes pour être activée
            if (cmd[2] !== undefined && savedBtn[cmd[2]] !== undefined) {
                btnElt.dataset.linkedTo = savedBtn[cmd[2]].id;
                savedBtn[cmd[2]].dataset.linkedBtn = btnElt.id;
                btnElt.disabled = true;
                // Rajoute un handler quand on clique sur le bouton de la commande nécessaire
                // ce qui permet de rendre actif/inactif le bouton de la commande courante
                savedBtn[cmd[2]].addEventListener("click", handlePreviousCommandNeeded(
                    savedBtn[cmd[2]],
                    btnElt
                ));
            }

            btnElt.addEventListener("click", genCmdSender(btnElt, spanElt, method, cmd[0], cmd[1]));

            savedBtn[cmd[0]] = btnElt;
        }

        newMethodElt.appendChild(gridElt);

        cmdMenu.appendChild(newMethodElt);
    }

    // Construit la liste des parametres que l'on peut configurer pour les
    // méthodes de captation
    for (method in config) {

        let newMethodElt = document.createElement("aside");

        let headerElt = document.createElement("header");
        headerElt.textContent = method;

        let spanElt = document.createElement("span");
        spanElt.classList.add("unfold");
        spanElt.textContent = "▼";
        spanElt.id = method.toLowerCase().replaceAll(' ', '') + "Menu";

        headerElt.appendChild(spanElt);
        newMethodElt.appendChild(headerElt);

        let articleElt = document.createElement("article");
        articleElt.classList.add("hidden");
        articleElt.id = method.toLowerCase().replaceAll(' ', '') + "Menu";

        spanElt.addEventListener("click", (evt) => {
            evt.preventDefault();
            if (spanElt.textContent == "▲") {
                spanElt.textContent = "▼";
            } else {
                spanElt.textContent = "▲";
            }
            articleElt.classList.toggle("hidden");
        });

        let configsContainer = document.createElement("form");
        configsContainer.classList.add("grid-wrapper");

        let configElements = constructMethodConfiguration(configsContainer, method, config[method]);

        let divElt = document.createElement("div");

        let btnValidation = document.createElement("input");
        btnValidation.classList.add("validate-config-btn");
        btnValidation.type = "submit";
        btnValidation.value = "validate";
        btnValidation.addEventListener("click", (evt) => {
            evt.preventDefault();

            let methodName = configElements[0][0].id.split("_")[0];
            let attrs = [];
            let vals = [];
            for (element of configElements[0]) {
                attrs.push(element.id.split("_").slice(1).join("_"));

                let value = element.value;

                if (element.type == "number") {
                    value = Number(element.value);
                } else if (element.type == "checkbox") {
                    value = element.checked;
                }

                vals.push(value);
            }

            changeConfig(methodName, attrs, vals);

        });

        divElt.appendChild(btnValidation);
        configsContainer.appendChild(divElt);

        configsContainer.appendChild(document.createElement("div"));
        configsContainer.appendChild(document.createElement("div"));
        configsContainer.appendChild(document.createElement("div"));

        articleElt.appendChild(configsContainer);
        newMethodElt.appendChild(articleElt);

        confAlgo.appendChild(newMethodElt);
    }

    if (sharesWithOther()) {
        shareInfo();
        socket.emit("changeShareStatus", { status: shareConfiguration });
        return
    }

    configChangeDisable(true);

});

socket.addEventListener("confirm", (evt) => {
    console.log(`Changement de ${evt.msgType}`, evt.ok ? "effectué" : "refusé");
    if ("font" in evt) {
        const size = evt.font.font_size + parseInt(smallForm.value, 10) - defaultFontSize;
        setFontSettings(evt.font.axe1, evt.font.axe2, evt.font.axe3, evt.font.axe4, evt.font.axe5, evt.font.axe6, evt.font.axe7, evt.font.axe8, evt.font.color, size);
    }
    if ("config" in evt) {
        let method = evt.config.method;
        let attrs = evt.config.attrs;
        let newVals = evt.config.newVals;
        for (let i = 0; i < attrs.length - 1; i++) {
            let elt = document.getElementById(method + "_" + attrs[i]);
            elt.value = newVals[i];
        }
    }
});

function constructMethodConfiguration(parentElement, method, config) {
    let elements = []
    for (paramName in config) {
        let theConfig = config[paramName]

        let divEltLabel = document.createElement("div");
        divEltLabel.id = "box1";
        let newParameterLabel = document.createElement("label");
        newParameterLabel.textContent = paramName.replace(/_/g, " ").replace('variable ', '');

        let newParameterInput = null

        divEltInput = document.createElement("div");

        // Selon la configuration reçu, on n'aura pas le même type d'input à générer

        if ("range" in theConfig) {

            divEltLabel.id = "box2";
            divEltInput.classList.add("select-wrapper");

            newParameterInput = document.createElement("select");
            for (value of theConfig["range"]) {
                let opt = document.createElement("option");
                opt.value = value;
                opt.textContent = value;
                if (value == theConfig["val"]) {
                    opt.selected = true
                }
                newParameterInput.appendChild(opt);
            }
        } else {
            switch (typeof (theConfig["val"])) {
                case "number":
                    newParameterInput = document.createElement("input");

                    if ("min" in theConfig && "max" in theConfig) {
                        newParameterInput.type = "range";
                        newParameterInput.min = theConfig["min"];
                        newParameterInput.max = theConfig["max"];
                    } else {
                        newParameterInput.type = "number";

                        if ("min" in theConfig) {
                            newParameterInput.min = theConfig["min"];
                        }

                        if ("max" in theConfig) {
                            newParameterInput.max = theConfig["max"];
                        }
                    }

                    newParameterInput.value = theConfig["val"];
                    break;
                case "string":
                    newParameterInput = document.createElement("input");
                    newParameterInput.type = "text";
                    newParameterInput.value = theConfig["val"];
                    break;
                case "boolean":
                    newParameterInput = document.createElement("input");
                    newParameterInput.type = "checkbox";
                    newParameterInput.checked = theConfig["val"];
                    break;
            }
        }

        if (!newParameterInput) {
            continue;
        }

        if ("description" in theConfig) {
            newParameterLabel.title = theConfig["description"];
            newParameterInput.title = theConfig["description"];
        }

        let newElementId = `${method}_${paramName}`;
        newParameterInput.id = newElementId;
        newParameterLabel.for = newElementId;

        divEltLabel.appendChild(newParameterLabel);
        parentElement.appendChild(divEltLabel);

        divEltInput.appendChild(newParameterInput);
        parentElement.appendChild(divEltInput);

        parentElement.appendChild(document.createElement("div"));
        parentElement.appendChild(document.createElement("div"));

        elements.push(parentElement);
    }

    return elements;
}

function changeConfig(method, attrs, newValues) {
    let data = {
        "method": method,
        "attrs": attrs,
        "newVals": newValues
    };
    if (shareConfiguration) {
        data["shares"] = true;
    }
    socket.emit("conf", data);
}

function changeFontConfig(axe1, axe2, axe3, axe4, axe5, axe6, axe7, axe8, color, fontSize) {
    let data = {
        "axe1": axe1,
        "axe2": axe2,
        "axe3": axe3,
        "axe4": axe4,
        "axe5": axe5,
        "axe6": axe6,
        "axe7": axe7,
        "axe8": axe8,
        "color": color,
        "font_size": fontSize,
    };
    if (shareConfiguration) {
        data["shares"] = true;
    }
    socket.emit("fontconf", data);
}

/* -----------
      Camera
   -----------*/

function sendImage() {
    if (camera.recording()) {
        width = videoElt.width;
        height = videoElt.height;
        context.drawImage(videoElt, 0, 0, width, height);
        let data = {
            "cam": formCam.checked,
            "img": canvas.toDataURL("image/jpeg", 0.5),
        }
        context.clearRect(0, 0, width, height);
        socket.emit("image", data);
    }
}

socket.addEventListener("processed", (data) => {
    if ("me" in data) {
        if ("img" in data.me) {
            myPhoto.setAttribute("src", data.me.img);
        }
        if ("font" in data.me) {
            const sizeMe = data.me.font.font_size + parseInt(smallForm.value, 10) - defaultFontSize;
            animateFont(myFontArea, data.me.font.axe1, data.me.font.axe2, data.me.font.axe3, data.me.font.axe4, data.me.font.axe5, data.me.font.axe6, data.me.font.axe7, data.me.font.axe8, data.me.font.color, sizeMe);
        }
    }
    if ("other" in data && profile2Status.classList.contains("btn-activated")) {
        if ("img" in data.other) {
            if (otherFontArea.classList.contains("hidden") && otherVideoContainer.classList.contains("hidden")) {
                otherNoCam.classList.add("hidden");
                otherNoCam.classList.remove("circle");
                otherVideoContainer.classList.remove("hidden");
            }
            otherPhoto.setAttribute("src", data.other.img);
        }
        if ("font" in data.other) {
            const sizeOther = data.other.font.font_size + parseInt(bigForm.value, 10) - defaultFontSize;
            animateFont(otherFontArea, data.other.font.axe1, data.other.font.axe2, data.other.font.axe3, data.other.font.axe4, data.other.font.axe5, data.other.font.axe6, data.other.font.axe7, data.other.font.axe8, data.other.font.color, sizeOther);
        }
    }
});

socket.addEventListener("cam_status", (data) => {
    for (var key in data) {
        if (profile2Status.classList.contains("btn-activated")) {
            if (data[key] == "no-cam") {
                otherNoCam.classList.remove("hidden");
                otherNoCam.classList.add("circle");
                otherVideoContainer.classList.add("hidden");
                otherFontArea.classList.add("hidden");
            } else if (data[key] == "video") {
                otherNoCam.classList.add("hidden");
                otherNoCam.classList.remove("circle");
                otherVideoContainer.classList.remove("hidden");
                otherFontArea.classList.add("hidden");
            } else {
                otherNoCam.classList.add("hidden");
                otherNoCam.classList.remove("circle");
                otherVideoContainer.classList.add("hidden");
                otherFontArea.classList.remove("hidden");
            }
        } else { // A mettre si on veut revenir au défaut quand on arrête le partage de feedback
            otherNoCam.classList.add("hidden");
            otherNoCam.classList.remove("circle");
            otherVideoContainer.classList.add("hidden");
            otherFontArea.classList.remove("hidden");
        }
    }
});

/* -----------
      Pseudos
   -----------*/

pseudoBtn.addEventListener("click", () => {
    if (pseudoBtn.textContent == "share") {
        pseudoVar.textContent = pseudoInput.value;
        pseudoBtn.textContent = "hide";
        changeElementColor(pseudoStatus);
        socket.emit("pseudo", pseudoInput.value);
    } else {
        pseudoVar.textContent = " ";
        pseudoBtn.textContent = "share";
        changeElementColor(pseudoStatus);
        socket.emit("pseudo", " ");
    }
});

pseudoInput.addEventListener("change", () => {
    if (pseudoBtn.textContent == "delete") {
        pseudoVar.textContent = pseudoInput.value;
        socket.emit("pseudo", pseudoVar.textContent);
    }
});

socket.addEventListener("pseudos", (data) => {
    for (var key in data) {
        otherPseudoVar.textContent = data[key];
    }
});

/* --------------------
          Partage
   --------------------*/

shareConfigBtn.addEventListener("click", () => {
    if (shareConfiguration) {
        shareInfo();
    }

    socket.emit("changeShareStatus", { status: shareConfiguration });
});

function shareInfo() {
    socket.emit("formSize", {
        smallForm: /([0-9\.]*)/.exec(window.getComputedStyle(myFontArea).fontSize)[1],
        bigForm: /([0-9\.]*)/.exec(window.getComputedStyle(otherFontArea).fontSize)[1]
    });
    // partage la configuration actuelle à l'autre
    for (btn of document.getElementsByClassName("validate-config-btn")) {
        if (!sharesWithOther()) {
            btn.disabled = false;
            btn.click();
            btn.disabled = true;
        }
    }
    socket.emit("formOrCam", { status: formCam.checked });
    socket.emit("hideBtn", { show: hideBtn.textContent === "show" });
    socket.emit("syncCaptations");

    otherFontArea.style.fontFamily = myFontArea.style.fontFamily;

    let data = {
        "name": myFontArea.style.fontFamily,
        "data": currentFont,
        "shares": shareConfiguration,
    }

    socket.emit("font_file", data);
}

socket.addEventListener("changeShareStatus", (data) => {
    if (sharesWithOther())
        return

    configChangeDisable(data.status);
});

/* --------------------------
   Invisibilité des boutons
   --------------------------*/

hideBtn.addEventListener("click", () => {
    if (shareConfiguration) {
        socket.emit("hideBtn", { show: hideBtn.textContent === "show" });
    }
});

socket.addEventListener("hideBtn", (data) => {
    // Si les boutons sont dans un état différent de ceux de l'autre utilisateur, ça va changer
    if ((hideBtn.textContent === "show") ^ data.show) {
        if (!sharesWithOther()) {
            hideBtn.disabled = false;
            hideBtn.click();
            hideBtn.disabled = true;
        }

    }
});

/* --------------------
   Quel type de partage
   --------------------*/

formCam.addEventListener("click", () => {
    if (shareConfiguration) {
        socket.emit("formOrCam", { status: formCam.checked });
    }
});

socket.addEventListener("formOrCam", (data) => {
    if (formCam.checked ^ data.status) {
        if (!sharesWithOther()) {
            formCam.disabled = false;
            formCam.click();
            formCam.disabled = true;
        }
    }
});

/* --------------------
     Change la taille
   --------------------*/

smallForm.addEventListener("change", () => {
    if (shareConfiguration) {
        socket.emit("formSize", {
            smallForm: window.getComputedStyle(myFontArea).fontSize
        });
    }
});

bigForm.addEventListener("change", () => {
    if (shareConfiguration) {
        socket.emit("formSize", {
            bigForm: window.getComputedStyle(otherFontArea).fontSize
        });
    }
});

socket.addEventListener("formSize", (data) => {
    if ("smallForm" in data) {
        smallForm.value = /([0-9]*)/.exec(data["smallForm"])[1];
        setSmallFormSize(data["smallForm"]);
    }

    if ("bigForm" in data) {
        bigForm.value = /([0-9]*)/.exec(data["bigForm"])[1];
        setBigFormSize(data["bigForm"]);
    }
});

/* --------------------
      Profile 2 status
   --------------------*/

socket.addEventListener("profile2status", () => {
    changeElementColor(profile2Status);
});

/* -----------
      Reset
   -----------*/

resetAxesBtn.addEventListener("click", () => {
    changeFontConfig(100, 100, 100, 100, 100, 100, 100, 100, [0, 0, 0], defaultFontSize);
});

resetSettingBtn.addEventListener("click", () => {
    const data = { "shares": shareConfiguration }

    socket.emit("reset", data);
});

socket.addEventListener("reset", (data) => {
    for (method in data) {
        for (paramName in data[method]) {
            const eltId = `${method}_${paramName}`;
            const elt = document.getElementById(eltId);

            if (elt) {
                const theConfig = data[method][paramName];
                if ("range" in theConfig) {

                    for (opt of elt.getElementsByTagName("option")) {
                        if (opt.value === theConfig["val"]) {
                            opt.selected = true;
                        } else {
                            opt.selected = false;
                        }
                    }

                } else {
                    switch (typeof (theConfig["val"])) {
                        case "number":
                            elt.value = theConfig["val"];
                            break;
                        case "string":
                            elt.value = theConfig["val"];
                            break;
                        case "boolean":
                            elt.checked = theConfig["val"];
                            break;
                    }
                }
            }
        }

    }

    smallForm.value = 80;
    setSmallFormSize("10vh");
    bigForm.value = 500;
    setBigFormSize("60vh");
});

socket.addEventListener("resetAllCaptations", () => {
    const btnElts = document.getElementsByClassName("btn-activate-captation");
    const spanElts = document.getElementsByClassName("picto-activate-captation");

    for (idx = 0; idx < btnElts.length; idx++) {
        if (spanElts[idx].classList.contains("btn-activated")) {
            btnElts[idx].disabled = false;
            btnElts[idx].click();
            btnElts[idx].disabled = true;
        }
    }
});


/* -----------
      Init Other
   -----------*/

socket.addEventListener("init_other", (data) => {
    if (data && data.is_first) {
        window.location.reload();
    } else {
        if (!sharesWithOther()) {
            configChangeDisable(true);
            myFontArea.style.fontFamily = "OrganicformVF";
        }

        socket.emit("pseudo", pseudoVar.textContent);
        let data = {
            "camStatus": getCamStatus(),
            "fontData": {
                "name": myFontArea.style.fontFamily,
                "data": currentFont,
                "shares": true,
            }
        }
        socket.emit("profile2status", data);
    }
});

/* -----------
      Disconnect Other
   -----------*/

socket.addEventListener("disconnect_other", () => {
    configChangeDisable(false);

    if (profile2Status.classList.contains("btn-activated")) {
        changeElementColor(profile2Status);
        otherNoCam.classList.add("hidden");
        otherNoCam.classList.remove("circle");
        otherVideoContainer.classList.add("hidden");
        otherFontArea.classList.remove("hidden");
    }
    if (shareConfiguration) {
        otherFontArea.style.fontFamily = currentFont;
    } else {
        otherFontArea.style.fontFamily = "OrganicformVF";
    }
    otherPseudoVar.textContent = " ";
    animateFont(otherFontArea, 100, 100, 100, 100, 100, 100, 100, 100, [0, 0, 0], parseInt(bigForm.value, 10));
});

/* -----------
      Font file
   -----------*/

typefaceBtn.addEventListener("click", async () => {
    typefaceText.textContent = "font file";

    const selectedFile = typefaceFiles.files[0];
    const font_data = await selectedFile.arrayBuffer();

    let selectedFileName = selectedFile.name;
    selectedFileName = selectedFileName.replace(".ttf", "");
    selectedFileName = selectedFileName.replace(".woff", "");
    selectedFileName = selectedFileName.replace(".woff2", "");

    currentFont = font_data

    font = new FontFace(selectedFileName, font_data);
    document.fonts.add(font);
    await font.load();

    myFontArea.style.fontFamily = `${selectedFileName}`;
    fontIcon.style.fontFamily = `${selectedFileName}`;
    if (shareConfiguration) {
        otherFontArea.style.fontFamily = `${selectedFileName}`;
    }

    let data = {
        "name": selectedFileName,
        "data": font_data,
        "shares": shareConfiguration,
    }

    socket.emit("font_file", data);
});

socket.addEventListener("font_file", async (data) => {
    for (var key in data) {
        if (profile2Status.classList.contains("btn-activated")) {
            let fontName = data[key]["name"];
            if (fontName != "OrganicformVF" && fontName) {
                font = new FontFace(fontName, data[key]["data"]);
                document.fonts.add(font);
                await font.load();
            }

            otherFontArea.style.fontFamily = `${fontName}`;
            if (data[key]["shares"]) {
                myFontArea.style.fontFamily = `${fontName}`;
                currentFont = data[key]["data"];
            }
        }
    }
});

/* -----------
      Utils
   -----------*/

function changeElementColor(elt) {
    elt.classList.toggle("btn-deactivated");
    elt.classList.toggle("btn-activated");
}

function changeElementText(elt) {
    if (elt.textContent == "activate") {
        elt.textContent = "disable"
    } else {
        elt.textContent = "activate"
    }
}

function handlePreviousCommandNeeded(btnNeeded, btnCommand) {
    return () => {
        if (btnNeeded.textContent === "activate") {
            if (btnCommand.textContent === "disable") {
                btnCommand.click();
            }
            btnCommand.disabled = true;
        } else {
            btnCommand.disabled = false;
        }
    }
}

function toRGB(hexColor) {
    res = []

    // Enlève le # de début
    hexColor = hexColor.slice(1)

    for (let colIdx = 0; colIdx < 3; colIdx++) {
        res[colIdx] = parseInt(hexColor.slice(colIdx * 2, colIdx * 2 + 2), 16)
    }

    return res
}

function computeFontSizeInVh(nbPx) {
    // On défini que 500px => 60vh; 
    // il suffit de faire un produit en croix pour déterminer la taille en vh pour une taille en
    // px donnée
    return pxToVh(nbPx * vhToPx(defaultFontSizeInVh) / defaultFontSize);
}

function sharesWithOther() {
    return Array(...shareConfigBtnGlb).every(elt => !elt.classList.contains("hidden"));
}

function configChangeDisable(status) {
    const configElt = document.getElementById("configuration");

    const ins = [
        configElt.getElementsByTagName("input"),
        configElt.getElementsByTagName("button"),
        configElt.getElementsByTagName("select")
    ]

    for (elts of ins) {
        for (elt of elts) {
            if (elt.dataset.linkedTo) {
                let linkedBtn = document.getElementById(elt.dataset.linkedTo);
                if (linkedBtn.textContent === "activate") {
                    elt.disabled = true;
                }
            } else {
                elt.disabled = status;
            }
        }
    }

    const form_slider = document.getElementById("form_slider");
    const file_input = document.getElementsByClassName("file-style");

    if (status) {
        file_input[0].classList.add("disabled");
        form_slider.classList.add("disabled");
    } else {
        file_input[0].classList.remove("disabled");
        form_slider.classList.remove("disabled");
    }
}