let shareConfiguration = true;

hideBtn.addEventListener("click", () => {
  if (hideBtn.textContent == "show") {
    hideBtn.textContent = "hide";
    changePictoVisibility(fullscreen);
    changePictoVisibility(config);
    changePictoVisibility(info);
    changePictoVisibility(profile);
  } else {
    hideBtn.textContent = "show";
    changePictoVisibility(fullscreen);
    changePictoVisibility(config);
    changePictoVisibility(info);
    changePictoVisibility(profile);
  }
});

fullscreen.addEventListener("click", () => {
  if (fullscreen.textContent == "$") {
    openFullscreen();
  } else {
    closeFullscreen();
  }
});

// Pour tous les menus définis, on ajoute un évènement pour quitter le menu
for (let elt of document.getElementsByClassName("overlay")) {
  elt.addEventListener("click", (evt) => {
    // Si l'élément cliqué correspond au vide autour du menu
    if (evt.target.classList.contains("overlay")) {
      document.location = "#";
    }
  });
}

// Change le pictogramme pour mettre en plein écran dans le cas ou l'on quitte le plein écran avec ECHAP
document.addEventListener("fullscreenchange", (evt) => {
  if (fullscreen.textContent == "$") {
    fullscreen.textContent = "%";
  } else {
    fullscreen.textContent = "$";
  }
});

typefaceFiles.addEventListener("change", () => {
  const selectedFile = typefaceFiles.files[0];
  typefaceText.textContent = selectedFile.name;
});

/* ----------------------
    taille des formes
   ---------------------*/

smallForm.addEventListener("change", () => {
  if (!((parseInt(smallForm.value, 10) > 0) && (parseInt(smallForm.value, 10) < 100)))
    smallForm.value = 80;
  setSmallFormSize(`${smallForm.value}pt`);
});

bigForm.addEventListener("change", () => {
  if (!((parseInt(bigForm.value, 10) > 100) && (parseInt(bigForm.value, 10) < 1000)))
    bigForm.value = 500;
  setBigFormSize(`${bigForm.value}pt`);
});

/* ---------------------
    Partage des config
   --------------------*/

shareConfigBtn.addEventListener("click", () => {
  shareConfiguration ^= true;
});

/* -----------
      Avatar
   -----------*/

avatarBtn.addEventListener("click", () => {
  let status = getCamStatus();

  if (avatarBtn.textContent == "activate") {

    avatarBtn.textContent = "disable";
    changeElementColor(avatarStatus);

    if (status == "form") {
      myFontArea.classList.remove("hidden");
      myVideoContainer.classList.add("hidden");
      myNoCam.classList.add("hidden");
      myNoCam.classList.remove("circle");
    } else if (status == "video") {
      myFontArea.classList.add("hidden");
      myVideoContainer.classList.remove("hidden");
      myNoCam.classList.add("hidden");
      myNoCam.classList.remove("circle");
    } else {
      myFontArea.classList.add("hidden");
      myVideoContainer.classList.add("hidden");
      myNoCam.classList.remove("hidden");
      myNoCam.classList.add("circle");
    }

  } else {

    avatarBtn.textContent = "activate";
    changeElementColor(avatarStatus);

    if (status == "form") {
      myFontArea.classList.add("hidden");
    } else if (status == "video") {
      myVideoContainer.classList.add("hidden");
    } else {
      myNoCam.classList.add("hidden");
      myNoCam.classList.remove("circle");
    }

  }

  pseudoVar.classList.toggle("hidden");
});

/* -----------
      Utils
   -----------*/

function changePictoVisibility(elt) {
  elt.classList.toggle("show");
  elt.classList.toggle("hide");
}

function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) { /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE11 */
    document.msExitFullscreen();
  }
}

function getCamStatus() {
  if (camera.recording() && formCam.checked) {
    return "video"
  }
  if (!camera.recording() && formCam.checked) {
    return "no-cam"
  }
  return "form"
}

function setSmallFormSize(newSize) {
  myFontArea.style.fontSize = newSize;
  myPhoto.style.width = newSize;
  myPhoto.style.height = newSize;
  myNoCam.style.width = newSize;
  myNoCam.style.height = newSize;
}

function setBigFormSize(newSize) {
  otherFontArea.style.fontSize = newSize;
  otherPhoto.style.width = newSize;
  otherPhoto.style.height = newSize;
  otherNoCam.style.width = newSize;
  otherNoCam.style.height = newSize;
}