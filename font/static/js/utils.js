function pxToVh(nbPx) {
    size = nbPx * 100 / window.innerHeight
    if (size < 0)
        size = 0
    return size
}

function vhToPx(nbVh) {
    size = nbVh * window.innerHeight / 100
    if (size < 0)
        size = 0
    return size
}
