const elem = document.documentElement;

// Menu control
const confAlgo = document.getElementById("conf_algo");
const cmdMenu = document.getElementById("cmd");

const unfoldElt = document.querySelectorAll(".unfold");
const generalMenu = document.getElementById("generalMenu");

// Font control
const otherFontArea = document.getElementById("other_font");
const myFontArea = document.getElementById("my_font");

// Icons control
const hideBtn = document.getElementById("hide");
const config = document.getElementById("config_icon");
const fullscreen = document.getElementById("fullscreen_icon");
const profile = document.getElementById("profile_icon");
const info = document.getElementById("info_icon");
const formCam = document.getElementById("form_cam");

// pseudo control
const pseudoBtn = document.getElementById("pseudo");
const pseudoInput = document.getElementsByName("pseudo")[0];
const pseudoVar = document.getElementById("my_pseudo_space");
const otherPseudoVar = document.getElementById("other_pseudo_space");
const pseudoStatus = document.getElementById("pseudo_status");

// avatar control
const shareConfigBtn = document.getElementById("config_sharing");
const shareConfigBtnGlb = document.getElementsByClassName("share-global");
const avatarBtn = document.getElementById("avatar");
const avatarStatus = document.getElementById("avatar_status");
const profile2Status = document.getElementById("profile2_status");

// Camera control
const camsList = document.getElementById("cams");
const camBtn = document.getElementById("cam");
const camStatus = document.getElementById("cam_status");

// Video display
const otherPhoto = document.getElementById("other_photo");
const otherNoCam = document.getElementById("other_no_cam");
const otherVideoContainer = document.getElementById("other_video_container");
const myPhoto = document.getElementById("my_photo");
const myNoCam = document.getElementById("my_no_cam");
const myVideoContainer = document.getElementById("my_video_container");
const videoElt = document.querySelector("#videoElement");
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

// Reset font
const resetBtn = document.getElementsByName("resetBtn");
const resetSettingBtn = document.getElementById("reset_setting_btn");
const resetAxesBtn = document.getElementById("reset_axes_btn");

// Typeface 
const typefaceBtn = document.getElementById("typefaceBtn");
const typefaceFiles = document.getElementById("typefaceFile");
const typefaceText = document.getElementById("file_text");
const fontIcon = document.getElementById("font_icon");

// Font size
const smallForm = document.getElementById("small_form");
const bigForm = document.getElementById("big_form");