const camera = new Camera(videoElt);

camera.getCameraSelection(camsList);

camBtn.addEventListener("click", () => {
	if (camBtn.textContent == "activate") {
		if (!camera.recording()) {
			camera.start(camsList.value);
			if (camera.ready()) {
				return
			}
		}
		camBtn.textContent = "stop";
		changeElementColor(camStatus);
		if (!myNoCam.classList.contains("hidden")) {
			myVideoContainer.classList.remove("hidden");
		}
		myNoCam.classList.add("hidden");
		myNoCam.classList.remove("circle");
	} else {
		camBtn.textContent = "activate";
		changeElementColor(camStatus);
		camera.stop();
		if (!myVideoContainer.classList.contains("hidden")) {
			myNoCam.classList.remove("hidden");
			myNoCam.classList.add("circle");
		}
		myVideoContainer.classList.add("hidden");
	}
	let status = getCamStatus();
	socket.emit("camStatus", status);
});

formCam.addEventListener("click", () => {
	if (avatarStatus.classList.contains("btn-activated")) {
		if (camera.recording() && formCam.checked) {
			myNoCam.classList.add("hidden");
			myNoCam.classList.remove("circle");
			myVideoContainer.classList.remove("hidden");
			myFontArea.classList.add("hidden");
		} else if (!camera.recording() && formCam.checked) {
			myVideoContainer.classList.add("hidden");
			myNoCam.classList.remove("hidden");
			myNoCam.classList.add("circle");
			myFontArea.classList.add("hidden");
		} else {
			myVideoContainer.classList.add("hidden");
			myNoCam.classList.add("hidden");
			myNoCam.classList.remove("circle");
			myFontArea.classList.remove("hidden");
		}
	}

	let status = getCamStatus();
	socket.emit("camStatus", status);
});

/* -----------
	  Utils
   -----------*/

function changeElementColor(elt) {
	elt.classList.toggle("btn-deactivated");
	elt.classList.toggle("btn-activated");
}
