import base64
from threading import Lock

import cv2
import numpy as np


# DEFAULT_FONT_SIZE = 5000
DEFAULT_FONT_SIZE = 500


def base64_to_image(base64_string):
    # Extrait les données binaires encodées en base64 depuis la chaine
    base64_data = base64_string.split(",")[1]
    # Décode la base64 en octet
    image_bytes = base64.b64decode(base64_data)
    # Converti les octets en matrice numpy
    image_array = np.frombuffer(image_bytes, dtype=np.uint8)
    # Décode la matrice numpy en image
    image = cv2.imdecode(image_array, cv2.IMREAD_COLOR)
    return image


class FontHandler:
    def __init__(self):
        self._axe1 = 100
        self._axe2 = 100
        self._axe3 = 100
        self._axe4 = 100
        self._axe5 = 100
        self._axe6 = 100
        self._axe7 = 100
        self._axe8 = 100
        # Couleur dans une liste dans l'ordre RGB
        self._color = [0, 0, 0]
        self._font_size = DEFAULT_FONT_SIZE

        self._started = False

        self._processors = {}

        self._lock = Lock()

    # -------------
    # Properties

    @property
    def font_config(self):
        return {
            "axe1": self._axe1,
            "axe2": self._axe2,
            "axe3": self._axe3,
            "axe4": self._axe4,
            "axe5": self._axe5,
            "axe6": self._axe6,
            "axe7": self._axe7,
            "axe8": self._axe8,
            "color": self._color,
            "font_size": self._font_size,
        }

    # -------------
    # Handlers

    def init(self):
        keys, configs = {}, {}
        for proc_name, proc in self._processors.items():
            if len(proc.cmd) != 0:
                keys[proc_name] = proc.cmd
            if len(proc.config) != 0:
                configs[proc_name] = proc.config

        return {"font": self.font_config, "keys": keys, "config": configs}

    def get_config(self):
        configs = {}
        for proc_name, proc in self._processors.items():
            if len(proc.config) != 0:
                configs[proc_name] = proc.config

        return configs

    def get_cmd(self):
        keys = {}
        for proc_name, proc in self._processors.items():
            if len(proc.cmd) != 0:
                keys[proc_name] = proc.get_active_cmd()

        return keys

    def receive_init_config(self, config_received):
        for method in config_received:
            if method in self._processors:
                attrs = [cmd for cmd in config_received[method]]
                new_vals = [
                    config_received[method][cmd]["val"]
                    for cmd in config_received[method]
                ]
            self._processors[method].process_config(attrs, new_vals)

    def receive_init_cmd(self, cmd_received):
        for method in cmd_received:
            if method in self._processors:
                for cmd in cmd_received[method]:
                    self._processors[method].process_cmd(cmd)

    def receive_cmd(self, cmd_received):
        if cmd_received["method"] in self._processors:
            self._processors[cmd_received["method"]].process_cmd(cmd_received["cmd"])

        return None

    def receive_font_config(self, font_received):
        self._lock.acquire()
        self.change_font(**font_received)
        self._lock.release()

        return None

    def receive_config(self, config_received):
        if config_received["method"] in self._processors:
            return self._processors[config_received["method"]].process_config(
                config_received["attrs"], config_received["newVals"]
            )
        return False

    def receive_image(self, data):
        b64img = data["img"]
        image = base64_to_image(b64img)

        if data["cam"]:
            # Encode l'image en base 64
            param_encoding = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
            _, frame = cv2.imencode(".jpg", image, param_encoding)
            proc_img_data = base64.b64encode(frame).decode()

            # Formatte correctement les données envoyées pour que le navigateur sache comment l'image est encodée
            b64_mime = "data:image/jpg;base64,"
            proc_img_data = b64_mime + proc_img_data

            return {"img": proc_img_data}

        self._lock.acquire()

        if len(self._processors) <= 0:
            self._lock.release()
            return None

        font_config = self.font_config
        for _, processor in self._processors.items():
            processor.process_frame(image)
            font_config = processor.process_font(font_config)
        self.change_font(**font_config)
        self._lock.release()
        return {"font": font_config}

    def receive_reset_config(self):
        for _, processor in self._processors.items():
            processor.process_reset_config()

        return None

    def receive_copy_config(self, config):
        for proc_name, processor in self._processors.items():
            processor.process_copy_config(config[proc_name])

    # -------------
    # Methods

    def change_font(
        self,
        axe1=-1,
        axe2=-1,
        axe3=-1,
        axe4=-1,
        axe5=-1,
        axe6=-1,
        axe7=-1,
        axe8=-1,
        color=[],
        font_size="",
    ):
        self._axe1 = axe1 if axe1 != -1 else self._axe1
        self._axe2 = axe2 if axe2 != -1 else self._axe2
        self._axe3 = axe3 if axe3 != -1 else self._axe3
        self._axe4 = axe4 if axe4 != -1 else self._axe4
        self._axe5 = axe5 if axe5 != -1 else self._axe5
        self._axe6 = axe6 if axe6 != -1 else self._axe6
        self._axe7 = axe7 if axe7 != -1 else self._axe7
        self._axe8 = axe8 if axe8 != -1 else self._axe8
        self._color = color if len(color) != 0 else self._color
        self._font_size = font_size if font_size else self._font_size

    def add_image_processor(self, processor):
        if not self._started:
            self._processors[processor.name] = processor

    def start(self):
        self._started = True

    def stop(self):
        self._started = False
