class Users:
    def __init__(self):
        self.usrs = []

    def next_usr_index(self):
        nb = 0

        while True:
            usr_i = 0
            while usr_i < len(self.usrs) and self.usrs[usr_i].nb != nb:
                usr_i += 1

            if usr_i >= len(self.usrs):
                return nb
            nb += 1

    def add_usr(self, usr_id):
        self.usrs.append(User(usr_id, self.next_usr_index()))
        return self.usrs[-1]

    def get_usr(self, usr_id):
        usr_i = 0
        while usr_i < len(self.usrs) and self.usrs[usr_i].id != usr_id:
            usr_i += 1

        if usr_i < len(self.usrs):
            return self.usrs[usr_i]
        return None

    def get_usr_by_idx(self, usr_idx):
        usr_i = 0
        while usr_i < len(self.usrs) and self.usrs[usr_i].nb != usr_idx:
            usr_i += 1

        if usr_i < len(self.usrs):
            return self.usrs[usr_i]
        return None

    def rm_usr(self, usr_id):
        usr_i = 0
        while usr_i < len(self.usrs) and self.usrs[usr_i].id != usr_id:
            usr_i += 1

        if usr_i < len(self.usrs):
            self.usrs = self.usrs[:usr_i] + self.usrs[usr_i + 1 :]


class User:
    def __init__(self, id, nb):
        self.id = id
        self.nb = nb
        self.info = {}
