import logging
from threading import Lock, Thread

from flask import Flask, render_template, request, redirect, url_for
from flask_socketio import SocketIO, emit, join_room, leave_room

from font.font_handler import FontHandler
from font.user import Users
from utils.logger import add_log_file, create_logger


class WebServer:
    def __init__(self, host="0.0.0.0", port=5000):
        self._serverHost = host
        self._serverPort = port

        self._logger = create_logger(__name__)
        add_log_file(self._logger, __name__, logging.CRITICAL)

        self._thread = None
        self._thread_lock = Lock()

        self._all_cli = []
        self._exchange_clients = {}
        self._exchange_clients_info = Users()
        self._processors_list = []

        async_mode = None

        self._app = Flask(__name__)
        self._socketio = SocketIO(self._app, async_mode=async_mode)

        self._app.add_url_rule("/", "index", self.index)
        self._app.add_url_rule("/observer", "observer", self.observer)

        self._socketio.on_event("connect", self.socket_connect)
        self._socketio.on_event("disconnect", self.socket_disconnect)
        self._socketio.on_event("cmd", self.font_cmd)
        self._socketio.on_event("fontconf", self.font_config)
        self._socketio.on_event("conf", self.config)
        self._socketio.on_event("image", self.font_img)
        self._socketio.on_event("pseudo", self.pseudo)
        self._socketio.on_event("profile2status", self.profile2status)
        self._socketio.on_event("camStatus", self.cam_status)
        self._socketio.on_event("font_file", self.font_file)
        self._socketio.on_event("reset", self.config_reset)
        self._socketio.on_event("formSize", self.share_form_size)
        self._socketio.on_event("formOrCam", self.share_form_or_cam_status)
        self._socketio.on_event("hideBtn", self.share_hidden_buttons)
        self._socketio.on_event("changeShareStatus", self.share_toggle)
        self._socketio.on_event("syncCaptations", self.share_sync_captations)

        self._idx = 0

    # --------------
    # Routes

    def index(self):
        with self._thread_lock:
            # Si on a déjà deux clients de connectés, on redirige le nouveau client vers le mode observateur
            if len(self._exchange_clients) >= 2:
                return redirect(url_for("observer"))
            return render_template("index.html")

    def observer(self):
        with self._thread_lock:
            # Si on n'a pas déjà deux clients de connectés, on redirige le nouveau client vers la page principale
            if len(self._exchange_clients) < 2:
                return redirect(url_for("index"))
            return render_template("observer.html")

    # ---------------
    # Handlers

    def socket_connect(self):
        sid = request.sid
        join_room(sid)

        self._logger.info(f'[SOCKET] new connexion from "{sid}"')

        with self._thread_lock:
            other = self.get_other_client(sid)
            self._all_cli.append(sid)

            # On ajoute un observateur
            if len(self._exchange_clients) >= 2:
                for cli in self._exchange_clients.keys():
                    emit("init_other", to=cli)
                return

            # On ajoute un client utilisateur
            new_client = FontHandler()
            new_usr = self._exchange_clients_info.add_usr(sid)

            for proc in self._processors_list:
                new_client.add_image_processor(proc())

            new_client.start()
            self._exchange_clients[sid] = new_client
            init = new_client.init()

            data = {"init": init, "config": "", "cmd": ""}

            if new_usr.nb == 0:  # On vient de créer celui qui contrôle
                data["is_first"] = True
                emit("init", data, to=sid)
                if other:
                    emit("init_other", data, to=other)

            elif (
                other and new_usr.nb != 0
            ):  # Il y a un autre utilisateur qui est celui qui contrôle
                data["config"] = self._exchange_clients[other].get_config()
                new_client.receive_init_config(data["config"])
                data["cmd"] = self._exchange_clients[other].get_cmd()
                new_client.receive_init_cmd(data["cmd"])
                emit("init_other", to=other)
                emit("init", data, to=sid)

                # On récupère les infos de celui qui contrôle
                usr_0 = self._exchange_clients_info.get_usr_by_idx(0)
                if "toggle" in usr_0.info:
                    emit("changeShareStatus", usr_0.info["toggle"], to=sid)
                    if not usr_0.info["toggle"]["status"]:
                        return

                if "formSize" in usr_0.info:
                    emit("formSize", usr_0.info["formSize"], to=sid)

                if "formOrStatus" in usr_0.info:
                    emit("formOrCam", usr_0.info["formOrStatus"], to=sid)

                if "hidden" in usr_0.info:
                    emit("hideBtn", usr_0.info["hidden"], to=sid)

    def socket_disconnect(self):
        sid = request.sid

        self._logger.info(f'[SOCKET] "{sid}" disconnect')

        with self._thread_lock:
            if sid in self._exchange_clients:
                other = self.get_other_client(sid)
                if len(self._exchange_clients) >= 2:
                    observers = []
                    for cli in self._all_cli:
                        if not (cli in self._exchange_clients.keys()):
                            observers.append(cli)

                    data = {}
                    usr = self._exchange_clients_info.get_usr(sid)
                    data["user"] = "usr1" if usr.nb == 0 else "usr2"

                    emit("disconnect_other", data, to=observers)

                if other:
                    emit("disconnect_other", to=other)

                self._exchange_clients[sid].stop()
                del self._exchange_clients[sid]

            self._all_cli.remove(sid)
            self._exchange_clients_info.rm_usr(sid)
            leave_room(sid)

    def font_cmd(self, cmd_received):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[CMD] "{request.sid}" send command')

            try:
                self._exchange_clients[request.sid].receive_cmd(cmd_received)

                with self._thread_lock:
                    usr = self._exchange_clients_info.get_usr(request.sid)

                    if "cmd" not in usr.info:
                        usr.info["cmd"] = []

                    try:
                        cmd_idx = usr.info["cmd"].index(cmd_received["cmdName"])
                        usr.info["cmd"] = (
                            usr.info["cmd"][:cmd_idx] + usr.info["cmd"][cmd_idx + 1 :]
                        )
                    except:
                        usr.info["cmd"].append(cmd_received["cmdName"])

                    if "shares" in cmd_received:
                        other = self.get_other_client(request.sid)
                        if other:
                            self._logger.info(f'[CMD] "{other}" received command')
                            try:
                                emit("cmd", cmd_received["cmdName"], to=other)
                            except Exception as e:
                                self._logger.critical(
                                    f'[CMD] OTHER : "{other}"', exc_info=e
                                )

            except Exception as e:
                self._logger.critical(f'[CMD] "{request.sid}"', exc_info=e)

    def font_config(self, font_received):
        shares_config = False
        if request.sid in self._exchange_clients.keys():
            if "shares" in font_received:
                shares_config = True
                # Supprime le champ "shares" sinon il va poser problème dans la méthode "receive_font_config"
                del font_received["shares"]

            self._logger.info(f'[FONT_CONFIG] "{request.sid}" send new font config')

            try:
                self._exchange_clients[request.sid].receive_font_config(font_received)
                emit(
                    "confirm",
                    {
                        "msgType": "fontconf",
                        "ok": True,
                        "font": self._exchange_clients[request.sid].font_config,
                    },
                    to=request.sid,
                )

                if shares_config:
                    with self._thread_lock:
                        other = self.get_other_client(request.sid)
                        if other:
                            self._logger.info(
                                f'[FONT_CONFIG] "{other}" received new font config'
                            )
                            try:
                                self._exchange_clients[other].receive_font_config(
                                    font_received
                                )
                                emit(
                                    "confirm",
                                    {
                                        "msgType": "fontconf",
                                        "ok": True,
                                        "font": self._exchange_clients[
                                            other
                                        ].font_config,
                                    },
                                    to=other,
                                )
                            except Exception as e:
                                self._logger.critical(
                                    f'[FONT_CONFIG] OTHER : "{other}"', exc_info=e
                                )

            except Exception as e:
                self._logger.critical(f'[FONT_CONFIG] "{request.sid}"', exc_info=e)

    def config(self, config_received):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[CONFIG] "{request.sid}" send new config')

            try:
                ok = self._exchange_clients[request.sid].receive_config(config_received)
                emit("confirm", {"msgType": "conf", "ok": ok}, to=request.sid)

                if "shares" in config_received:
                    with self._thread_lock:
                        other = self.get_other_client(request.sid)
                        if other:
                            self._exchange_clients_info.get_usr(request.sid).info[
                                "config"
                            ] = config_received

                            self._logger.info(f'[CONFIG] "{other}" received new config')
                            try:
                                ok = self._exchange_clients[other].receive_config(
                                    config_received
                                )
                                emit(
                                    "confirm",
                                    {
                                        "msgType": "conf",
                                        "ok": ok,
                                        "config": config_received,
                                    },
                                    to=other,
                                )
                            except Exception as e:
                                self._logger.critical(
                                    f'[CONFIG] OTHER : "{other}"', exc_info=e
                                )

            except Exception as e:
                self._logger.critical(f'[CONFIG] "{request.sid}"', exc_info=e)

    def font_img(self, data):
        if not (request.sid in self._exchange_clients.keys()):
            return

        usr = self._exchange_clients_info.get_usr(request.sid)
        data_usr = "usr1" if usr.nb == 0 else "usr2"

        try:
            data_me = self._exchange_clients[request.sid].receive_image(data)
            if not data_me:
                return

            emit("processed", {"me": data_me}, to=request.sid)

            with self._thread_lock:
                other_cli_sid = self.get_other_client(request.sid)
                if other_cli_sid is not None:
                    emit("processed", {"other": data_me}, to=other_cli_sid)

                observers = []
                for cli in self._all_cli:
                    if not (cli in self._exchange_clients.keys()):
                        observers.append(cli)
                emit("observed", {data_usr: data_me}, to=observers)

        except Exception as e:
            self._logger.critical(f'[IMG] "{request.sid}"', exc_info=e)

    def pseudo(self, pseudo):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[PSEUDO] "{request.sid}" changes pseudo')
            with self._thread_lock:
                usr = self._exchange_clients_info.get_usr(request.sid)
                data_usr = "usr1" if usr.nb == 0 else "usr2"

                data = {}
                data[data_usr] = pseudo

                # On envoie à tout le monde sauf celui qui a envoyé
                self._socketio.emit("pseudos", data, include_self=False)

    def profile2status(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[PROFILE2STATUS] "{request.sid}" changes status')
            with self._thread_lock:
                other = self.get_other_client(request.sid)
                usr = self._exchange_clients_info.get_usr(request.sid)
                data_usr = "usr1" if usr.nb == 0 else "usr2"

                data_cam = {}
                data_cam[data_usr] = data["camStatus"]

                data_font = {}
                data_font[data_usr] = data["fontData"]

                if other:
                    self._socketio.emit("profile2status", include_self=True)

                self._socketio.emit("cam_status", data_cam, include_self=False)
                self._socketio.emit("font_file", data_font, include_self=False)

    def cam_status(self, cam_status):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[CAM] "{request.sid}" changes cam status')
            with self._thread_lock:
                usr = self._exchange_clients_info.get_usr(request.sid)
                data_usr = "usr1" if usr.nb == 0 else "usr2"

                data = {}
                data[data_usr] = cam_status

                self._socketio.emit("cam_status", data, include_self=False)

    def font_file(self, font_data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[FONT_FILE] "{request.sid}" changes font_file')
            with self._thread_lock:
                usr = self._exchange_clients_info.get_usr(request.sid)
                data_usr = "usr1" if usr.nb == 0 else "usr2"

                data = {}
                data[data_usr] = font_data

                self._socketio.emit("font_file", data, include_self=False)

    def config_reset(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[RESET] "{request.sid}" reset config')
            with self._thread_lock:
                me = self._exchange_clients[request.sid]

                try:
                    me.receive_reset_config()
                    me_new_config = me.get_config()

                    emit("reset", me_new_config, to=request.sid)

                    if "shares" in data and data["shares"]:
                        other = self.get_other_client(request.sid)
                        if other:
                            self._logger.info(
                                f'[RESET] "{other}" received reset config'
                            )
                            try:
                                other_font_handler = self._exchange_clients[other]
                                other_font_handler.receive_reset_config()
                                other_config = other_font_handler.get_config()
                                emit("reset", other_config, to=other)
                            except Exception as e:
                                self._logger.critical(
                                    f'[RESET] OTHER : "{other}"', exc_info=e
                                )

                except Exception as e:
                    self._logger.critical(f'[RESET] "{request.sid}"', exc_info=e)

    def share_form_size(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[SHARE FORM SIZE] "{request.sid}" changes form size')
            with self._thread_lock:
                self._exchange_clients_info.get_usr(request.sid).info["formSize"] = data
                other = self.get_other_client(request.sid)
                if other:
                    emit("formSize", data, to=other)

    def share_form_or_cam_status(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(
                f'[SHARE FORM STATUS] "{request.sid}" changes form of cam status'
            )
            with self._thread_lock:
                self._exchange_clients_info.get_usr(request.sid).info[
                    "formOrStatus"
                ] = data
                other = self.get_other_client(request.sid)
                if other:
                    emit("formOrCam", data, to=other)

    def share_hidden_buttons(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[SHARE HIDDEN] "{request.sid}" hides/shows buttons')
            with self._thread_lock:
                self._exchange_clients_info.get_usr(request.sid).info["hidden"] = data
                other = self.get_other_client(request.sid)
                if other:
                    emit("hideBtn", data, to=other)

    def share_toggle(self, data):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(f'[SHARE TOGGLE] "{request.sid}" toggles share button')
            with self._thread_lock:
                self._exchange_clients_info.get_usr(request.sid).info["toggle"] = data
                other = self.get_other_client(request.sid)
                observers = []
                for cli in self._all_cli:
                    if not (cli in self._exchange_clients.keys()):
                        observers.append(cli)

                emit("changeShareStatus", to=observers)

                if other:
                    emit("changeShareStatus", data, to=other)

    def share_sync_captations(self):
        if request.sid in self._exchange_clients.keys():
            self._logger.info(
                f'[SHARE CAPTATIONS] "{request.sid}" requests sync with captations'
            )
            with self._thread_lock:
                other = self.get_other_client(request.sid)
                if other:
                    self._logger.info(
                        f'[SHARE CAPTATIONS] "{other}" received sync captations request'
                    )
                    try:
                        emit("resetAllCaptations", to=other)
                        usr = self._exchange_clients_info.get_usr(request.sid)
                        if usr and "cmd" in usr.info:
                            for cmd in usr.info["cmd"]:
                                emit("cmd", cmd, to=other)

                        me = self._exchange_clients[request.sid]
                        me_config = me.get_config()
                        other_font_handler = self._exchange_clients[other]
                        other_font_handler.receive_copy_config(me_config)
                        emit("reset", me_config, to=other)
                    except Exception as e:
                        self._logger.critical(
                            f'[SHARE CAPTATIONS] OTHER : "{other}"', exc_info=e
                        )

    # ----------------
    # Methods

    def get_other_client(self, me):
        for other in self._exchange_clients.keys():
            if other != me:
                return other
        return None

    def add_image_processor(self, processor_constructor):
        processor_idx = 0
        while (
            processor_idx < len(self._processors_list)
            and self._processors_list[processor_idx].__name__
            != processor_constructor.__name__
        ):
            processor_idx += 1

        if processor_idx >= len(self._processors_list):
            self._processors_list.append(processor_constructor)

    def _run(self):
        try:
            print(f"Server starting at {self._serverHost}:{self._serverPort}")
            self._socketio.run(
                self._app,
                host=self._serverHost,
                port=self._serverPort,
                allow_unsafe_werkzeug=True,
            )
        except Exception as e:
            self._logger.critical(f"[THREAD RUN] ", exc_info=e)

    def start(self):
        with self._thread_lock:
            if self._thread is None:
                self._thread = Thread(target=self._run)

                try:
                    self._thread.start()
                except Exception as e:
                    self._logger.critical(f"[THREAD START] ", exc_info=e)
