# Menu configuration

## General

### Form <> camera

Permet de passer de la forme à la font. Si la caméra n'est pas active et qu'on met le switch sur caméra, on affiche l'icône caméra. Le changement est également retransmis aux observateurs. Il est aussi retransmis à l'autre participant si `avatar feedback` est activé.

### Buttons

Permet d'enlever l'affichage des icônes de menu aux coins supérieurs de l'écran (icône de configuration et icône de plein-écran).   
*Les icônes ne sont pas réellement enlevées, mais mis en blanc. On peut encore y accéder et cliquer dessus.*

### Settings

Permet de rafraîchir la page.   
C'est utile pour revenir à l'état par défaut, notamment de la taille des formes, des axes correspondant aux différentes captations et des valeurs associées aux captations. 

### Font axes

Permet de remettre à la valeur par défaut les valeurs sur les axes de la font.

## Typeface

### Font file

Permet de sélectionner un fichier de font (parmi ceux sur l'ordinateur) à utiliser pour les formes.   
Les fichiers acceptés sont `.ttf`, `.woff`, `.woff2`. La font est aussi changée pour le retour du participant chez les observateurs, et chez l'autre participant si `avatar feedback` est activé. QUand on sélectionne un fichier, le nom est affiché à la place de `font file`. Le fichier est chargé quand on appuie sur `load`.

## Detector

Permet de sélectionner quelle.s captation.s activer.   
Chacune change les axes (sauf `head distance` qui change la taille de la font).

### Expressions

* `eyebrows` : haussement des sourcils (1 axe) ;
* `mouth` : longueur et hauteur de la bouche (2 axes) ;
* `head distance` : distance à l'écran ;
* `head move` : mouvement de la tête de profil, et de haut en bas (4 axes).

### Heart pulse

* `pulse` : active/désactive la captation cardiaque (2 axes) ;
* `color` : captation cardiaque visible par variation de couleur (plus le battement est élevé, plus la couleur tend vers le rouge vif).

**Attention** : le mode `color` n'est activable qu'une fois le mode `pulse` activé.

## Settings

### General

#### Big form

Permet de changer la taille du retour de l'autre participant.   
Le changement est pris en compte quand on appuie sur entrée ou qu'on déplace le curseur en dehors de la zone de texte.

**Attention** : la valeur doit être comprise entre 100 et 1000, sinon la font revient à sa valeur par défaut (et le texte dans la zone de texte aussi).

#### Small form

Permet de changer la taille du retour du participant courant.   
Le changement est pris en compte quand on appuie sur entrée ou qu'on déplace le curseur en dehors de la zone de texte.

**Attention** : la valeur doit être comprise entre 0 et 100, sinon la font revient à sa valeur par défaut (et le texte dans la zone de texte aussi).

### Expressions

Permet de changer les paramètres liés aux captations des expressions du visage.   
Les changements sont pris en compte quand on clique sur le bouton `validate`.
*(Il est possible d'avoir une description des paramètres en laissant la souris sur les noms).*   

#### Number field

Permet de régler des paramètres liés à la captation.

##### Sourcils

* `brows down` : distance entre le sourcil et la paupière basse quand on fronce les sourcils au maximum ;
* `brows up` : distance entre le sourcil et la paupière basse quand on hausse les sourcils au maximum ;

##### Bouche

* `open mouth min` : distance minimale entre le haut et le bas de la bouche ;
* `open mouth max` : distance maximale entre le haut et le bas de la bouche ;

* `smile min` : différence minimale entre les extrémités droite et gauche de la bouche ;
* `smile max` : différence maximale entre les extrémités droite et gauche de la bouche ;

##### Distance du bout du nez à l'écran

* `distance min` : distance à l'écran minimale ;
* `distance max` : distance à l'écran maximale ;

##### Tête

* `head y min` : angle de rotation minimum de la tête sur le côté ;
* `head y max` : angle de rotation maximum de la tête sur le côté ;

* `head x min` : angle de rotation minimum de la tête à la verticale ;
* `head x max` : angle de rotation maximum de la tête à la verticale.

#### Select field (axes)

Permet de changer l'axe de la font associé à la captation.

##### Sourcils

* `brows` : variable de la font associée aux sourcils ;

##### Bouche

* `open mouth` : variable de la font associée à la hauteur de la bouche ;
* `smile` : variable de la font associée à la longueur de la bouche ;

##### Distance du bout du nez à l'écran

* `distance` : variable de la font associée à la distance à l'écran ;

##### Tête

* `head up` : variable de la font associée à la tête vers le haut ;
* `head down` : variable de la font associée à la tête vers le bas ;

* `head left` : variable de la font associée au profil gauche ;
* `head right` : variable de la font associée au profil droit.

### Heart pulse

Permet de changer les paramètres liés aux captations du pouls.   
*(il est possible d'avoir une description des paramètres en laissant la souris sur les noms).*   
Les changements sont pris en compte quand on clique sur le bouton `validate`.

* `font_min` : valeur minimale de déformation de la font ;
* `font_max` : valeur maximale de déformation de la font.

# Menu profile

## Profile 1

### Camera

Permet d'activer la caméra.   
Quand la caméra n'est pas active, si on essaye de l'afficher, on affiche l'icône caméra dans un cercle.

**Attention** : si on bloque la caméra, le bouton ne reflète pas l'état de la caméra.

### Pseudo

Permet d'afficher le pseudo.   
Le pseudo est affiché une fois qu'on a appuyé sur `show`. Il est affiché pour les 2 participants et pour les observateurs. Quand on change le pseudo pendant qu'il est activé, les changements sont automatiquement transmis aux participants et aux observateurs sans avoir besoin d'appuyer sur un bouton (changements pris en compte quand on appuie sur entrée ou qu'on déplace le curseur en dehors de la zone de texte).

### Avatar feedback

Permet de transmettre le retour du participant (changements de forme avec la font sélectionnée, ou caméra) à l'autre participant.   
Quand il n'est pas activé, on affiche la font par défaut à l'autre participant (à savoir, le rond noir). Quand le participant se déconnecte, son retour revient automatiquement à la forme par défaut pour les tous les clients encore connectés. Si `avatar feedback` est activé, dès qu'un nouveau participant se connecte, il reçoit le bon retour.   
*Dans tous les cas, les observateurs ont accès aux changements.*

## Profile 2

Reflète si `avatar feedback` de l'autre participant est activé ou non.

# Observer

## Menu configuration

### Buttons

Permet d'enlever l'affichage des icônes de menu aux coins supérieurs de l'écran (icône de configuration et icône de plein-écran).   
*Les icônes ne sont pas réellement enlevées, mais mis en blanc. On peut encore y accéder et cliquer dessus.*

### Form size

Permet de changer la taille des retours des participants.   
Le changement est pris en compte quand on appuie sur entrée ou qu'on déplace le curseur en dehors de la zone de texte.

**Attention** : la valeur doit être comprise entre 100 et 1000, sinon la font revient à sa valeur par défaut (et le texte dans la zone de texte aussi).