import logging
import os


DIRECTORY = "log"


def create_logger(name, level=logging.INFO):
    logger = logging.getLogger(name)
    logger.setLevel(level)

    if not os.path.exists(DIRECTORY):
        os.makedirs(DIRECTORY)

    add_log_file(logger, name, level)

    return logger


def add_log_file(logger, name, level):
    formatter = get_formatter()
    file_handler = logging.FileHandler(create_logger_file_name(name, level))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    return logger


def create_logger_file_name(name, level):
    return f"{DIRECTORY}/{logging.getLevelName(level)}-{name}-logs.log"


def get_formatter():
    return logging.Formatter("%(asctime)s | %(levelname)s \n %(message)s")
