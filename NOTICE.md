Premier Contact   
Copyright 2023 Nathan Menny (https://github.com/nmenny), Laura Miguel (https://github.com/MiguelLaura), Simon Renaud

The Initial Developer of some parts of the application captations/pulse/utils, which are copied from, derived from, or inspired by, is Tristan Hearn (https://github.com/thearn).   
(https://github.com/thearn/webcam-pulse-detector/tree/no_openmdao)

The fonts (Augure-Light, Augure-LightSlanted, OrganicformVF and pictosicons-Regular) in 
font/static/css/fonts were designed by Simon Renaud.   
Copyright (c) 2023by . All rights reserved.

