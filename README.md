# Premier Contact : dispositif expérimental pour manifester la présence à distance

Captations :
* du visage
* de la fréquence cardiaque ([PulseDetector](https://github.com/thearn/webcam-pulse-detector))

## Installation

Commencer par cloner le projet.

### Avec Docker

#### Sans avoir cloné le repo

En récupérant l'image Docker sur gitlab :
```shell
docker run -p 5000:5000 registry.gitlab.utc.fr/mennynat/tx-premier-contact
```

#### En ayant cloné le repo

Pour utiliser Docker (le nom de l'image sera `NOM_DU_DOSSIER-web`) :
```shell
docker compose up
```

Ou en séparant les étapes :
```shell
# Créer l'image
docker image build -t NOM_IMAGE .
# Lancer le conteneur
docker run -p 5000:5000 NOM_IMAGE
```

### En ayant Python sur sa machine

Il faut avoir cloné le repo. Installer les dépendances qui sont dans le requirements.txt.

```shell
pip install -r requirements.txt
```

Lancer la demo sur la caméra + font et voir le résultat dans son navigateur :

```shell
python prog.py
```

# Utilisation

Ensuite, ouvrir http://localhost:5000 dans son navigateur. Attention, quand plusieurs personnes sont devant l'écran, on ne peut pas déterminer qui est pris en compte.

## Paramètres de ExpressionsDetector

### Sourcils

* brows_down : distance entre le sourcil et la paupière basse quand on fronce les sourcils au maximum
* brows_up : distance entre le sourcil et la paupière basse quand on hausse les sourcils au maximum
* variable_brows : variable de la font associée aux sourcils

### Bouche

* smile_min : différence minimale entre les extrémités droite et gauche de la bouche
* smile_max : différence maximale entre les extrémités droite et gauche de la bouche
* variable_smile : variable de la font associée à la longueur de la bouche

* open_mouth_min : distance minimale entre le haut et le bas de la bouche
* open_mouth_max : distance maximale entre le haut et le bas de la bouche
* variable_open_mouth : variable de la font associée à la hauteur de la bouche

### Tête

* head_y_min : angle de rotation minimum de la tête sur le côté
* head_y_max : angle de rotation maximum de la tête sur le côté
* variable_head_left : variable de la font associée au profil gauche
* variable_head_right : variable de la font associée au profil droit

* head_x_min : angle de rotation minimum de la tête à la verticale
* head_x_max : angle de rotation maximum de la tête à la verticale
* variable_head_up : variable de la font associée à la tête vers le haut
* variable_head_down : variable de la font associée à la tête vers le bas

### Distance du bout du nez à l'écran

* distance_min : distance à l'écran minimale
* distance_max : distance à l'écran maximale
* variable_distance : variable de la font associée à la distance à l'écran

## Commandes

### ExpressionsDetector

* Activer/désactiver les expressions des sourcils (change la variable `axe2`)
* Activer/désactiver les expressions de la bouche (la longueur de la bouche change la variable `axe3`, et la hauteur change `axe4`)
* Activer/désactiver la distance à l'écran (change la taille de la font)
* Activer/désactiver la direction de la tête (`axe1` quand on montre notre profil gauche, et `axe2` pour le droit, `axe3` pour la tête vers le haut et `axe4` pour la tête vers le bas)

### PulseDetector

* Activer/désactiver la captation du pouls
* Activer/désactiver le mode "couleur" de la font qui va faire varier l'intensité de rouge de la font selon l'intensité du battement cardiaque

Le mode "couleur" ne peut être activé que si l'application est en train de capter le pouls.

# Développer

Pour créer l'image Docker et la mettre dans le registry gitlab :
```shell
docker build -t registry.gitlab.utc.fr/mennynat/tx-premier-contact .
docker push registry.gitlab.utc.fr/mennynat/tx-premier-contact
```

Utiliser `black .` (et snake case en Python, camel case en JS).
Quand on ajoute une fonctionnalité, penser à la renseigner dans le [README.md](README.md).

> Imports should be grouped in the following order:
>
>    * standard library imports
>    * related third party imports
>    * local application/library specific imports

# Visage (captations/face_expressions)

## Fonctionnalités

* Bouche : différence entre les extrémités droite et gauche de la bouche (divisée par la différence entre les extrémités droite et gauche du visage, correspond à la longueur ou a `smile` dans le code) et la distance entre le haut et le bas de la bouche (correspond à `open_mouth` dans le code)
* Sourcils : différence entre le milieu du sourcil et le milieu de la paupière basse (moyenne entre le sourcil droit et gauche, divisée par la différence entre les extrémités droite et gauche du visage)
* Tête : calculs des rotations de la tête verticalement (`axe1` quand on montre notre profil gauche, et `axe2` pour le droit) et horizontalement (`axe3` pour la tête vers le haut et `axe4` pour la tête vers le bas)
* Distance : position sur l'axe z de notre nez


# Rythme cardiaque (captations/pulse)

## Fonctionnalités

Code qui détecte le rythme cardiaque de quelqu'un en utilisant une caméra.

* Une fois activée, la fréquence cardiaque va être estimée et selon l'intensité du battement, la font ou sa couleur sera modifiée proportionnellement.

### Principe de fonctionnement

Lorsque le code est en cours d'exécution, il va rechercher le visage de l'utilisateur·rise pour en isoler la région du front. Une fois cette zone isolée, le programme va estimer le rythme cardiaque de l'utilisateur·rice en regardant la variation moyenne du canal vert de cette zone de l'image (voir [reference](http://www.opticsinfobase.org/oe/abstract.cfm?uri=oe-16-26-21434)).

Les développeurs de cet outil ont estimé qu'avec une bonne luminosité et un minimum de mouvement de la part de l'utilisateur·rice, un rythme cardiaque stable devrait pouvoir être établi en une quinzaine de secondes. Puis, une fois cette phase terminée, le programme va continuer à estimer en temps réel le rythme cardiaque.
