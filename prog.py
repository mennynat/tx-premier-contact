from captations.face_expressions import ExpressionsDetector
from captations.pulse import PulseDetector
from font.web_server import WebServer

if __name__ == "__main__":
    s = WebServer()
    s.add_image_processor(ExpressionsDetector)
    s.add_image_processor(PulseDetector)
    s.start()
